# SuperTracer

SuperTracer is a [Mathematica](https://www.wolfram.com/mathematica/resources/) package aimed at facilitating the functional matching procedure for generic UV models. This package automates the most tedious parts of one-loop functional matching computations, namely the determination and evaluation of all relevant supertraces, including loop integration and Dirac algebra manipulations. The current version of SuperTracer also contains a limited set of output simplifications. However, a further reduction of the output to a minimal basis using Fierz identities, integration by parts, simplification of Dirac structures, and (or) light-field redefinitions might still be necessary.

If you use SuperTracer please cite: [arXiv:2012.08506](https://arxiv.org/abs/2012.08506).

## Installing and loading of the package

The simplest way to download and install SuperTracer is to run the following command in a Mathematica session:

> Import["https://gitlab.com/supertracer/supertracer/-/raw/master/install.m"]

This will download and install SuperTracer in the Applications folder of Mathematica's base directory. To load SuperTracer use the command:

> <<"SuperTracer`"

The complete set of routines and usage examples can be found in [arXiv:2012.08506](https://arxiv.org/abs/2012.08506). This repository also contains two notebook examples: _VLfermExample.nb_ and _S1LQExample.nb_.

## Authors

* **Javier Fuentes-Martin** - *Johannes Gutenberg University*
* **Matthias König** - *Technische Universität München*
* **Julie Pagès** - *University of Zurich*
* **Anders Eller Thomsen** - *University of Bern*
* **Felix Wilsch** - *University of Zurich*

## Bugs and feature requests

Please submit bugs and feature requests using GitLab's issue system.

## License

SuperTracer is free software under the terms of the GNU General Public License v3.0.


## Acknowledgments

We thank Timothy Cohen, Xiaochuan Lu, and Zhengkang Zhang for communications and cross-checks before the release of the package.

(* ::Package:: *)

InstallSuperTracer::version="Warning: `1` has only been tested on Mathematica versions \[GreaterEqual] `2` and you are using Mathematica `3`";


InstallSuperTracer:=Module[{packageName,packageDir,MinVersion,SuperTracerLink,QuestionOverwrite,tmpFile,unzipDir,zipDir},

	(* Definitions *)
	packageName="SuperTracer";
	packageDir=FileNameJoin[{$UserBaseDirectory,"Applications","SuperTracer"}];
	MinVersion=11.0;
	SuperTracerLink="https://gitlab.com/supertracer/supertracer/-/archive/master/supertracer-master.zip";

	(* Messages *)
	QuestionOverwrite="SuperTracer is already installed. Do you want to replace the content of "<>packageDir<>" with a newly downloaded version?";

	(* Check Mathematica version *)
	If[$VersionNumber<MinVersion,
		Message[InstallSuperTracer::version,packageName,ToString[MinVersion],$VersionNumber];
	];

	(* Check if SuperTracer has already been installed *)
	If[
		DirectoryQ[packageDir],
		If[
			ChoiceDialog[QuestionOverwrite,{"Yes"->True,"No"->False},WindowFloating->True,WindowTitle->"SuperTracer installation detected"],
			Quiet@DeleteDirectory[packageDir,DeleteContents->True],
			Abort[]
		];
	];

	(* Download SuperTracer *)
	Print["Downloading SuperTracer from ",SuperTracerLink];

	tmpFile=Quiet@URLSave[SuperTracerLink];

	If[tmpFile===$Failed,
		Print["Failed to download SuperTracer.\nInstallation aborted!"];
		Abort[]
	];

	(* Unzip SuperTracer file *)
	Print["Extracting SuperTracer zip file"];

	unzipDir=tmpFile<>".dir";
	ExtractArchive[tmpFile,unzipDir];

	(* Move files to the Mathematica packages folder *)
	Print["Copying SuperTracer to "<>packageDir];

	zipDir=FileNames["SuperTracer.m",unzipDir,Infinity];
	CopyDirectory[DirectoryName[zipDir[[1]]],packageDir];

	(* Delete the extracted archive *)
	Quiet@DeleteDirectory[unzipDir,DeleteContents->True];
	Print["Installation complete!"];

];


InstallSuperTracer;

### 2022-03-04
- Fixed bug in SuperSimplify with Levi-Civita tensors

### 2021-07-29
- Changed STrTerm to not automatically evaluate the Feynman integrals. The result is returned with functions LF[{propagator masses}, {propagator powers}], to simplify evaluation of (especially) multi-scale supertraces.
- Added function EvaluateLoopFunctions to evaluate all loop functions (LF) in an expression.

### 2021-06-23
- Fixed bug in field-strength tensors on internal gauge lines.
- All gauge indices on internal lines in the substitution rules should have the format \\[Alpha][i] or \\[Alpha][j] like in the example notebooks.

(* ::Package:: *)

(* ::Title:: *)
(*Initialization [SuperTracer`]*)


(* Generate warning for Mathematica versions earlier than 11.0.0 *)
If[$VersionNumber < 11.0, 
CellPrint[{
TextCell["SuperTracer` was developed for Mathematica 11.0.0 and later. 
Your current Mathematica version [" <> ToString@$Version <> "] might not be compatible. 
In case you are experiencing problems with SuperTracer`, please update your Mathematica version.",
"Text",Background->LightRed]
}]
]


(* Loading the package *)
If[MemberQ[$Packages,"SuperTracer`"],
	(* Avoid double loading the package *)
	Print[Style["The package SuperTracer` is already loaded. Please restart the kernel to reload it.",RGBColor[.8,.4706,0.2573]]],
	
	(* Set directory of AutoEFT package *)
	$DirectorySuperTracer= DirectoryName[$InputFileName, 2];
	$LogoSuperTracer= First@Import[FileNameJoin[{DirectoryName[$InputFileName],"LogoSuperTracer.pdf"}]];

	(* Loading *)
	(*Print["Loading SuperTracer`..."];*)
	Check[
		Get[FileNameJoin[{$DirectorySuperTracer, "SuperTracer.m"}]],
		Print[Style["Loading failed!",RGBColor[.6,.0706,0.1373]]];
		Abort[]
	];
	
	(*Print the logo*)
	CellPrint[ExpressionCell[$LogoSuperTracer,CellMargins->{{70,5},{5,5}}]];
	(*CellPrint[TextCell["SuperTracer","Text",Purple,Background->LightBlue,FontSize->14]];*)

	Print[
	"by Javier Fuentes-Mart\[IAcute]n, Matthias K\[ODoubleDot]nig, Julie Pag\[EGrave]s, Anders Eller Thomsen, and Felix Wilsch \n",
	"Reference: ",Hyperlink["arXiv:2012.08506","https://arxiv.org/abs/2012.08506"],"\n",
	"Website: ",Hyperlink["https://gitlab.com/supertracer/supertracer","https://gitlab.com/supertracer/supertracer"]
	];
];


(* Protect the symbols used in the package *)
(*
SetAttributes[
  Evaluate @ Flatten[Names /@ {"SuperTracer`*"}],
  {Protected, ReadProtected}
]
*)

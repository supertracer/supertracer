(* ::Package:: *)

Package["SuperTracer`"]


(* ::Title:: *)
(*SuperTracer`NCM`*)


(* ::Subtitle:: *)
(*Contains the definitions of the NonCommutativeMultiply*)


(* ::Chapter:: *)
(*Public:*)


(* ::Section:: *)
(*Scoping:*)


(* ::Subsubsection:: *)
(*Exported*)


PackageExport["G"]


(* ::Subsubsection:: *)
(*Internal*)


PackageScope["SetNonCommutative"]


PackageScope["CommutativeQ"]


PackageScope["Commutative"]


PackageScope["Commutator"]
PackageScope["AntiCommutator"]


PackageScope["ExtractMomenta"]


PackageScope["p"]
PackageScope["\[CapitalDelta]"]
PackageScope["dot"]
PackageScope["CollectPropagators"]
PackageScope["del"]
PackageScope["PerformMomDerivatives"]
PackageScope["GCommuteQ"]


(* ::Section:: *)
(*Usage definitions*)


G::usage=
"G[li[\[Mu],\[Nu]],li[<seq>]] represents a field-strength tensor with Lorentz indices \[Mu] and \[Nu]. The optional argument <seq> is a sequence of Lorentz indices which represent covariant derivatives acting on the field-strength tensor.";


Unprotect@NonCommutativeMultiply;
(* overwrite NonCommutativeMultiply *)
NonCommutativeMultiply::usage= 
"NonCommutativeMultiply[seq] is the non-commutative product of all elements in the sequance seq.";


(* ::Text:: *)
(*--- Internal ---*)


SetNonCommutative::usage=
"SetNonCommutative[obj] defines the object obj to be non-commutative, where obj can be a Sequence, List or Symbol.";


CommutativeQ::usage=
"CommutativeQ[obj] returns True if obj is a comutative object and false otherwise.";

Commutative::usage=
"Commutative[obj] is an auxiliary head that can be used to treat the object obj temporarily as commutative.";


Commutator::usage=
"Commutator[X,Y] gives the commutator of X and Y, i.e. [X,Y]=X**Y-Y**X.";

AntiCommutator::usage=
"AntiCommutator[X,Y] gives the anti-commutator of X and Y, i.e. {X,Y}=X**Y+Y**X.";


dot::usage=
"dot[a,b] represents the inner/dot product of a and b.";


CollectPropagators::usage=
"CollectPropagators collects all masses and \[CapitalDelta] in a non-commutative product.";


(* ::Section:: *)
(*Formatting*)


Format[G[li[\[Mu]_, \[Nu]_], li@ \[Rho]__], StandardForm]:= Row@ {Subscript[D, List@ \[Rho]], Subscript[G, \[Mu], \[Nu]]};
Format[G[li[\[Mu]_, \[Nu]_], li[]], StandardForm]:= Subscript[G, \[Mu], \[Nu]];


(* ::Chapter:: *)
(*Private:*)


(* ::Section:: *)
(*NonCommutativeMultiply functionality*)


(* ::Subsection:: *)
(*Commutative check*)


(* ::Text:: *)
(*Default assumption is that everything is commutative*)


CommutativeQ[f_?CommutativeQ[x___]] := And@@ CommutativeQ/@ {x};
CommutativeQ[f_[x___]] := False;
CommutativeQ[_] := True; 


(* ::Text:: *)
(*Define the non-commutative objects*)


CommutativeQ@ Alternatives[Pattern, Blank, BlankSequence, BlankNullSequence, Except] = False;


CommutativeQ@ List:= False; (*Matrices are non-commutative*)
CommutativeQ@ {_Index..}:= True; (*Index lists are commutative*)


SetNonCommutative[x_List]   := SetNonCommutative/@ x;
SetNonCommutative[x_, y__]  := (SetNonCommutative@ x; SetNonCommutative@ y;);
SetNonCommutative[x_Symbol] := (CommutativeQ@ x ^= False;);


(* ::Text:: *)
(*Commutative is a head that can be used to temporarily treat an object as commutative*)


CommutativeQ@ Commutative@ _ ^= True; (*Why upset?*)


(* ::Subsection:: *)
(*Properties of NonCommutativeMultiply*)


(* ::Text:: *)
(*Remove attributes problematic for pattern matching*)


ClearAttributes[NonCommutativeMultiply, {Flat, OneIdentity}];


(* ::Text:: *)
(*Set definitions for the non-commutative product*)


NonCommutativeMultiply[a___, NonCommutativeMultiply[b__], c___]:= NonCommutativeMultiply[a, b, c];
NonCommutativeMultiply[] = 1;
(*NonCommutativeMultiply[a_] := a;*) (*This is not needed it seems. It simplifies some edge cases, but this may not be benefitial.*)


NonCommutativeMultiply[a___, b_?CommutativeQ, c___]:= b * NonCommutativeMultiply[a, c];
NonCommutativeMultiply[a___, b_?CommutativeQ * x_, c___]:= b * NonCommutativeMultiply[a, x, c];
NonCommutativeMultiply[a___, b_Plus, c___]:= NonCommutativeMultiply[a, #, c] & /@ b (*There should be a Plus Head on the rhs. ?!*)


(* ::Subsubsection:: *)
(*Working on matrices and vectors*)


NonCommutativeMultiply@ expr_List:= expr;


NonCommutativeMultiply[a___, x_List, y_List, b___]:= 
	NonCommutativeMultiply[a, Inner[NonCommutativeMultiply, x, y, Plus], b];
NonCommutativeMultiply[a___, x_List, y: Except[_List], b___]:= 
	NonCommutativeMultiply[a, NonCommutativeMultiply[#, y]&/@ x, b];
NonCommutativeMultiply[a___, x: Except[_List], y_List, b___]:= 
	NonCommutativeMultiply[a, NonCommutativeMultiply[x, #]&/@ y, b];


(* ::Subsection:: *)
(*Utility functions*)


Commutator[x_, y_]     := x ** y - y ** x;
AntiCommutator[x_, y_] := x ** y + y ** x;


(* ::Subsection:: *)
(*Format*)


(* ::Text:: *)
(*Because of black magic and witchcraft this formatting must go hear *)


Format[NonCommutativeMultiply@x_, StandardForm]:= x;


(* ::Section:: *)
(*Symbols used in the CDE*)


(* ::Text:: *)
(*All non-commutative objects must be defined here to ensure proper evaluation order during package initialization.*)


SetNonCommutative[X, M, \[CapitalDelta]t, Xt, CovD]; (*From SuperTracer.m*)


SetNonCommutative[DiracProduct, \[Gamma]]; (*From DiracAlgebra.m*)


(* ::Subsection:: *)
(*Collect momentum structures*)


SetNonCommutative[p, \[CapitalDelta]];


(* ::Text:: *)
(*Bring momenta to propagator (with zero mass) form.*)


p/:Power[_p,2] := \[CapitalDelta]@0;
p/:dot[p,p]    := \[CapitalDelta]@0;


(* ::Text:: *)
(*Extract p-slashes from DriacProduct*)


(*ExtractMomenta@expr_ := expr//.{
		DiracProduct[\[Mu]___,p,\[Nu]___] :> Module[{\[Delta]},DiracProduct[\[Mu],li@\[Delta],\[Nu]] * p@li@\[Delta]]
	}/.{x:Alternatives[_p,\[CapitalDelta][0]]->Commutative@x}/.Commutative@x_->x; (*Should these rules not be delayed?*)*)


(* ::Text:: *)
(*The new version also replaces product of momentum vectors with symmetrized product of metrics, e.g. p@li@\[Mu] p@li@\[Nu] -> \[CapitalDelta][0] g@li[\[Mu],\[Nu]] /\[ScriptD], etc. *)


ExtractMomenta@expr_ := Module[{out, pInds},
	out= expr//.{
		DiracProduct[\[Mu]___,p,\[Nu]___] :> Module[{\[Delta]},DiracProduct[\[Mu],li@\[Delta],\[Nu]] * p@li@\[Delta]]
	}/.{x:Alternatives[_p,\[CapitalDelta][0]]->Commutative@x}/.Commutative@x_->x;
	Expand[out pInds[]]//. pInds@ x___ p@ \[Mu]_li:> pInds[x, \[Mu]]/. pInds-> SymmetricLoopMomReplacement// Expand
]


SymmetricLoopMomReplacement@ momInds___:= Module[{inds, n},
	inds= Flatten@ li@ momInds;
	n= Length@ inds/ 2; 
	SymTensor@ inds \[CapitalDelta][0]^n Gamma[\[ScriptD]/2]/ (Gamma[\[ScriptD]/2 + n] 2^n)
];


(* ::Text:: *)
(*Collect factors of Mass and \[CapitalDelta] inside the non-commutative product*)


CombineFactor/: NonCommutativeMultiply[a___, CombineFactor@ x_, CombineFactor@ y_, b___]:= NonCommutativeMultiply[a, CombineFactor[x*y], b];


CollectPropagators@ expr_:= expr/. {
		x:Alternatives[_\[CapitalDelta], Power[_\[CapitalDelta], _], _M, Power[_M, _]]-> CombineFactor@ x 
	}/. CombineFactor@ x_-> x;


(* ::Subsection:: *)
(*p-derivatives*)


SetNonCommutative@ del;


(* ::Text:: *)
(*Test if expression is independent of loop momentum (Subscript[p, \[Mu]]) *)


MomFreeQ[f_?MomFreeQ[x___]]:= And@@ MomFreeQ/@ {x}; 
MomFreeQ[f_[x___]]:= False;
MomFreeQ[_]:= True; 
MomFreeQ[p| \[CapitalDelta]]:= False; 


(* ::Text:: *)
(*Rules for applying a partial momentum derivative to an individual term*)


MomDerivative[_?MomFreeQ, _]:= 0;
MomDerivative[x_Plus, \[Mu]_]:= MomDerivative[#, \[Mu]]&/@ x;   
MomDerivative[Times[x_, y__], \[Mu]_]:= MomDerivative[x, \[Mu]] y + x MomDerivative[Times@ y, \[Mu]];
MomDerivative[Power[x_, n_], \[Mu]_]:= n Power[x, n-1] MomDerivative[x, \[Mu]];


MomDerivative[p@ li@ \[Mu]_, li@ \[Nu]_]:= g@ li[\[Mu], \[Nu]];
MomDerivative[Blank@\[CapitalDelta], \[Mu]_li]:= 2 p@ \[Mu];
MomDerivative[\[Gamma]@ p, \[Mu]_li]:= \[Gamma]@ \[Mu]; 


(* ::Text:: *)
(*Auxiliary function*)


LastPosition[x_, y_]:= Module[{temp},
	temp = FirstPosition[Reverse@ x, y, None, {1}];
	If[temp=== None, temp, Length@ x- First@ temp +1]
];


(* ::Text:: *)
(*Act with momentum derivatives to the right in an NCM product, and eventually terminate them.*)


TakeMomDerivatives@ expr_:= expr//. {
	NonCommutativeMultiply[a___, del@ \[Mu]_, x:Except[_del], b___]:> 
		NonCommutativeMultiply[a, x, del@ \[Mu], b] + NonCommutativeMultiply[a, MomDerivative[x, \[Mu]], b],
	NonCommutativeMultiply[___, _del]:> 0
};


(* ::Text:: *)
(*Perform all momentum derivatives in a loop integral expression using IbP to reduce the number of terms.  *)


PerformMomDerivatives@ expr_:= expr/. x_NonCommutativeMultiply:> PerformMomDerivatives@ x;
PerformMomDerivatives@ expr_NonCommutativeMultiply:= Module[{pos},
	pos= LastPosition[expr, _del];
	If[pos === None, Return@ expr];
	(*Decides whether to do IbP*)
	PerformMomDerivatives@ If[Count[expr[[;; pos- 1]], Except[_? MomFreeQ], {1}]- Count[expr[[pos+ 1;; ]], Except[_? MomFreeQ], {1}] > 0,
		expr[[;; pos- 1]] ** (TakeMomDerivatives@ expr[[pos;;]])   
	,
		pos= First@ FirstPosition[expr, _del, None, {1}];
		-TakeMomDerivatives[expr[[pos]]**expr[[;;pos-1]] ]** expr[[pos+1;;]] 
	]
];


(* ::Subsection:: *)
(*Field strength tensor*)


(* ::Text:: *)
(*Subscript[G, \[Mu]\[Nu]] properties. G[li[\[Mu], \[Nu]], li[\[Alpha]1,...] ] is identified with \!\(\*SubscriptBox[\(D\), \({\[Alpha]1,  ... }\)]\) Subscript[G, \[Mu]\[Nu]]*)


SetNonCommutative@ G;


(*Anti-symmetry of G*)
G[li[\[Mu]_,\[Mu]_],_li]=0;
G[li[\[Mu]_,\[Nu]_],li@OrderlessPatternSequence[\[Mu]_,\[Nu]_,___]] =0;


GCommuteQ[G|X|del|CovD]=False;
GCommuteQ[f_?GCommuteQ[x__]]:=And@@GCommuteQ/@{x};
GCommuteQ[f_[x__]]:=False;
GCommuteQ@ Alternatives[Pattern, _Blank, _BlankSequence, _BlankNullSequence, _Except] = False;
GCommuteQ[_]=True;


NonCommutativeMultiply[a___,G@\[Mu]__,x_?GCommuteQ,b___] := NonCommutativeMultiply[a,x,G@\[Mu],b];


CovD[\[Alpha]_li, G| _G, li[\[Mu]_, \[Mu]_]]:= 0;
CovD[li[\[Alpha]___, \[Mu]_, \[Nu]_], G| _G, li@ OrderlessPatternSequence[\[Mu]_, \[Nu]_]]:= 0;


G/: CovD[_, G@ {a_, a_}, _]:= 0;

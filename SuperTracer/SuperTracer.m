(* ::Package:: *)

Package["SuperTracer`"]


(* ::Title:: *)
(*SuperTracer`*)


(* ::Subtitle:: *)
(*Paclet for calculating the covariant derivative expansion (CDE) for use in functional matching methods.*)


(* ::Chapter:: *)
(*Public:*)


(* ::Section:: *)
(*Scoping*)


(* ::Subsubsection:: *)
(*Exported*)


PackageExport["PowerTerms"]
PackageExport["STr"]


PackageExport["STrTerm"]


PackageExport["LogTerm"]


PackageExport["\[CapitalPhi]"]
PackageExport["\[Phi]"]
PackageExport["\[CapitalPsi]"]
PackageExport["\[Psi]"]
PackageExport["A"]
PackageExport["V"]
PackageExport["cA"]
PackageExport["cV"]


PackageExport["LTerm"]


PackageExport["Xords"]


PackageExport["X"]


PackageExport["M"]


PackageExport["CD"]
PackageExport["CovD"]


PackageExport["SimplifyOutput"]


PackageExport["SuperSimplify"]


(* ::Subsubsection:: *)
(*Internal*)


PackageScope["CollectLTerms"]


PackageScope["\[CapitalDelta]t"]
PackageScope["Xt"]


PackageScope["TakeDev"]
PackageScope["TakeSymDev"]
PackageScope["IntegerSets"]
PackageScope["SplitSymmetrizedDevs"]
PackageScope["STrSymmetryFactor"]
PackageScope["TildeExpand"]
PackageScope["XInputCheck"]


(* ::Section::Closed:: *)
(*Usage messages*)


STr::usage=
"STr[{X1,..,XN}] denotes the header of supertraces of a list of X interaction from PowerTerms. These supertraces can be evaluated by applying the replacement /.STr->STrTerm. Symmetry factors and a global factor of -\!\(\*FractionBox[\(\[ImaginaryI]\), \(2\)]\) is included in the definition of Str.";


STrTerm::usage=
"StrTerm[XList, <order>, <substitution>] evaluates the power-type supertrace of a given list of X terms to a given order in the EFT expansion. The output is assumed to be inside a trace over internal degrees of freedom. Symmetry factors and a global factor of -\!\(\*FractionBox[\(\[ImaginaryI]\), \(2\)]\) are included in the supertrace definition. If the optional argument <order> is a number n (default: order=6), operators up to dimension n are provided, however if <order> is {n} only operators of dimension n are returned. The third argument is also optional and specifies substitutions for X, G and M that should be applied.";


LogTerm::usage=
"LogTerm[field, <order>] evaluates the logarithm-type supertrace resulting from integrating out a heavy fields to a given order in the EFT expansion. The field, which should be one of \[CapitalPhi] | \[CapitalPsi] | V | cV is assumed to be real (or Majorana in fermionic case), thus the result should be multiplied by two for complex (or Dirac) fields. If the optional argument <order> is a number n (default: order=6), operators up to dimension n are provided, however if <order> is {n} only operators of dimension n are returned.";


PowerTerms::usage=
"PowerTerms[Xlist, <order>,<XtermNumber>] returns all relevant power-type supertraces that need to be calculated for the given list Xlist of X-terms, including symmetry factors. If the optional argument <order> is a number n (default: <order>=6), operators up to dimension n are provided, however if <order> is {n} only operators of dimension n are returned. The optinal argument <XtermNumber> selects the supertraces with <XtermNumber> X terms on it. As for <order>, <XtermNumber> can either be of the form <XtermNumber>=n, with n a number, or <XtermNumber>={n}. The supertraces (Str) in the result can be evaluated by applying the replacement /.STr->STrTerm after running PowerTerm.";


\[CapitalPhi]::usage="\[CapitalPhi] denotes a heavy scalar field.";
\[Phi]::usage="\[Phi] denotes a light scalar field.";
\[CapitalPsi]::usage="\[CapitalPsi] denotes a heavy fermion field.";
\[Psi]::usage="\[Psi] denotes a light fermion field.";
V::usage="V denotes a heavy vector field.";
A::usage="A denotes a light vector field.";
cV::usage="cV denotes a heavy ghost field.";
cA::usage="cA denotes a light ghost field.";


LTerm::usage=
"LTerm[coeff,OpStr] separator for coefficients and the operator structure.";


Xords::usage=
"Xords is an association with the default interaction order for each X term.";


M::usage=
"M[label] is the mass of the heavy field associated to the given label.";


X::usage=
"X[{f1,f2}, <order>] is the input for the X interactions. The arguments f1 and f2 should be field types, while <order> is an optional argument specifying the order of the X. The <order> can either be a single number in the case of no open covariant derivatives or a list of numbers indicating the orders of terms in X with 0,1,2,... open covariant derivatives. If <order> is not given, the default values from Xords are assumed.";


CD::usage=
"CD[ind,expr] returns the covariant derivative(s) of a given expression expr. The argument ind can either be a single Lorentz index or a list of Lorentz indices for each of which a covariant derivative is applied to expr.";


CovD::usage=
"CovD[li[<seq1>],expr,li[seq2]] representation of a sequence of covariant derivatives specified by the optional argument <seq1> acting on the expression expr. The expression can have Lorentz indices that are given by the optional sequence <seq2>.
CovD[li[<seq>],Field[label,type,indices,charges],li[]] representation of a sequence of covariant derivatives specified by the optional argument <seq1> acting on the specified field with name given by label, type given by type, indices being a list of its indices and charges a list of its charges.
CovD[li[<seq>],G[gauge],li[\[Mu],\[Nu]]] representation of a field-strength tensor and a sequence of covariant derivatives, specified by the optional argument <seq1>, acting on it. The arguments \[Mu] and \[Nu] are the Lorentz indices of the field-strength tensor. The argument gauge indicates the gauge structure and can be either a string labeling the Abelian symmetries, or a list of two non-Abelian indices.";


SuperSimplify::usage=
"SuperSimplify[expr] applies multiple simplification algorithms on the given expression expr.";


SimplifyOutput::usage= 
"SimplifyOutput[expr] applies IbP identities and commutation relations to reduce the number of operators in an expression.";


(* ::Text:: *)
(*--- Intern ---*)


CollectLTerms::usage=
"CollectLTerms[expr] simplifies the expression expr, which is the output of STrTerm, by collecting terms that have the same operator structure.";


(* ::Section::Closed:: *)
(*Formatting*)


Do[
	Format[x@ i_, StandardForm] = Subscript[x, i];
	,{x, {\[CapitalPhi], \[CapitalPsi], \[Phi], \[Psi], A, V, cV, cA}}
]


Format[X@ {eta1_, eta2_}, StandardForm]:= Subscript[X, Style[Row@ {eta1, eta2}, FontSize-> 14]];
Format[X[{eta1_, eta2_}, ord_], StandardForm]:= Subsuperscript[X, Style[Row@ {eta1, eta2}, FontSize-> 13],Style["["<>ToString[ord,InputForm]<>"]",FontSize->9]];


Format[CovD[li[], op_, li[]], StandardForm]:= op;
Format[CovD[li[], op_, li@ \[Alpha]__], StandardForm]:= Superscript[op, Row@ List@ \[Alpha]];
Format[CovD[li@ \[Nu]__, op_, li[]], StandardForm]:= Module[{ind},
	Row@ Flatten@ {Table[Subscript[D, ind], {ind, {\[Nu]}}]//.
	{x___, Subscript[D, a_], Subscript[D, a_], y___}-> {x, D^2, y}, op}
];
Format[CovD[li@ \[Nu]__, op_, li@ \[Alpha]___], StandardForm]:= Module[{ind},
	Row@ Flatten@ {Table[Subscript[D, ind], {ind, {\[Nu]}}]//.
	{x___, Subscript[D, a_], Subscript[D, a_], y___}-> {x, D^2, y}, Superscript[op, Row@ List@ \[Alpha]]}
];


Format[LTerm[a_Plus, b_], StandardForm]:= Row@ {DisplayForm@ RowBox@ {"(", a, ")"}, b};
Format[LTerm[a_, b_], StandardForm]:= Row@ {a, b};
Format[LTerm[1, b_], StandardForm]:= b;


Format[M@ "H", StandardForm]:= Subscript[M, Style["H", FontSize-> 12]]
Format[M@ eta_, StandardForm]:= Subscript[M, Style[eta, FontSize-> 12]]


Format[X[{eta1_, eta2_}, li[], li[]], StandardForm]:= Subscript[X, Style[Row@ {eta1, eta2}, FontSize-> 14]];
Format[X[{eta1_, eta2_}, li@ \[Mu]__, li[]], StandardForm]:= Row@ {Subscript[D, List@ \[Mu]], Subscript[X, Style[Row@ {eta1, eta2}, FontSize-> 14]]};
Format[X[{eta1_, eta2_}, li[], li@\[Nu]__], StandardForm]:= Subscript[Superscript[X, Row@ List@ \[Nu]], Style[Row@ {eta1, eta2}, FontSize-> 14]];
Format[X[{eta1_, eta2_}, li@\[Mu]__, li@\[Nu]__], StandardForm]:= Row@ {Subscript[D, List@ \[Mu]], Subscript[Superscript[X, Row@ List@ \[Nu]], Style[Row@ {eta1, eta2}, FontSize-> 14]]};


(* Make abelian field-strength tensors display with F instead of G *)
Format[G@charge_Symbol, StandardForm]:= Subscript["F",charge];

Format[G@{ind1:Index[_,rep_], ind2:Index[_,rep_]}, StandardForm]:=Subsuperscript["G", ToString[rep],Row@{ind1,ind2}];


(* ::Chapter:: *)
(*Private:*)


(* ::Text:: *)
(*IntegerSet[s,n] returns all ordered sets of n integers {Subscript[\[Mu], 1],...,Subscript[\[Mu], n]}, such that Subscript[\[CapitalSigma], k] Subscript[\[Mu], k]=s and Subscript[\[Mu], k]>=0.*)


IntegerSets[sum_, ints_]:= Flatten[Permutations@ PadRight[#, ints]&/@
	DeleteCases[IntegerPartitions@ sum, _?(Length@ # > ints &)], 1];


(* ::Text:: *)
(*Generic orders for the various X types*)


Xords=Module[{tmpXords},
	tmpXords={
		{\[CapitalPhi],\[CapitalPhi]}->1,{\[CapitalPhi],\[Phi]}->1,{\[CapitalPhi],\[CapitalPsi]}->3/2,{\[CapitalPhi],\[Psi]}->3/2,{\[CapitalPhi],V}->{2,1} ,{\[CapitalPhi],A}->{3,2},{\[CapitalPhi],cV}->100,{\[CapitalPhi],cA}->100,
		{\[Phi],\[Phi]}->1 ,{\[Phi],\[CapitalPsi]}->3/2,{\[Phi],\[Psi]}->3/2,{\[Phi],V}->{2,1},{\[Phi],A}->{2,1} ,{\[Phi],cV}->100,{\[Phi],cA}->100,
		{\[CapitalPsi],\[CapitalPsi]}->1,{\[CapitalPsi],\[Psi]}->1,{\[CapitalPsi],V}->3/2 ,{\[CapitalPsi],A}->5/2 ,{\[CapitalPsi],cV}->100,{\[CapitalPsi],cA}->100,
		{\[Psi],\[Psi]}->1,{\[Psi],V}->3/2,{\[Psi],A}->3/2,{\[Psi],cV}->100,{\[Psi],cA}->100,
		{V,V}->2,{V,A}-> {2,3},{V,cV}->100,{V,cA}->100,
		{A,A}->2, {A,cV}->100,{A,cA}->100,
		{cV,cV}->1,{cV,cA}->{3,3},
		{cA,cA}->100
	};
	Join[tmpXords,Map[Reverse[#[[1]]]->#[[2]]&,tmpXords]]//Association
];


(* ::Section:: *)
(*Helper functions*)


(* ::Subsubsection:: *)
(*Organize output*)


(* ::Text:: *)
(*Operator test*)


NonOperatorQ[f_?NonOperatorQ[x___]] := And@@ NonOperatorQ/@ {x};
NonOperatorQ[f_[x___]] := False;
NonOperatorQ[_] := True;


NonOperatorQ@ Alternatives[Pattern, Blank, BlankSequence, BlankNullSequence, Except] = False;


NonOperatorQ@ Alternatives[NonCommutativeMultiply, CovD, \[CurlyEpsilon]]= False;


(* ::Text:: *)
(*Properties of the operator product*)


LTerm[a_, b_?NonOperatorQ x_]:= LTerm[a b, x];
LTerm[c_?NumberQ a_, x_]:= c LTerm[a, x];
LTerm[a_, c_?NumberQ]:= a c;
LTerm[a_, op_Plus]:= LTerm[a, #]&/@ op;
LTerm[0, _]:= 0;
LTerm[c:Except[1]?NumberQ, op_]:= c LTerm[1, op];


CollectLTerms@ expr_:= Module[{out},
	out= Expand[expr/. LTerm-> Times]/. Power[\[Epsilon], -1]-> 0;

	If[Head@ out =!= Plus,
		Return@ LTerm[1, out];
	];

	out= LTerm[1, #]&/@ out// LTermMatching;

	out/. LTerm[a_, x_]:> LTerm[Simplify@ a, x]
];



LTermMatching@ expr_:= Expand@ expr//. {
		LTerm[c1_, x_] + LTerm[c2_, x_]:> LTerm[c1+ c2, x],
		a1_ LTerm[c1_, x_] + LTerm[c2_, x_]:> LTerm[a1 c1+ c2, x],
		a1_ LTerm[c1_, x_] + a2_ LTerm[c2_, x_]:> LTerm[a1 c1+ a2 c2, x]
	}


(* ::Subsubsection::Closed:: *)
(*CDE expansion of propagators and X operators *)


(* ::Text:: *)
(*Extracts Subscript[\!\(\*OverscriptBox[\(\[CapitalDelta]\), \(~\)]\), \[Eta]]=(Subscript[\[CapitalSigma], m=1] (1/Subscript[\[CapitalDelta], \[Eta]] Subscript[\[ScriptCapitalG], \[Eta]])^m) 1/Subscript[\[CapitalDelta], \[Eta]]  to a given order*)


\[CapitalDelta]tBos[mass_, ord_]:= Module[{j, m, extraOrd, ords, count},
	Sum[
		extraOrd= IntegerSets[ord -2 -2m, m];
		Sum[
			count= 1;
			NonCommutativeMultiply@@ Table[\[CapitalDelta][mass]^(-1)** OGscal@ ords[[count++ ]], {j, m}]** \[CapitalDelta][mass]^(-1)
		,{ords, extraOrd}]
	,{m, 0, ord/2- 1}]
];

\[CapitalDelta]tFerm[mass_, ord_]:= Module[{j, m, orders, ords},
	Sum[
		orders= 1+ IntegerSets[ord-1-2m, m];
		Sum[
			NonCommutativeMultiply@@ Table[\[CapitalDelta][mass]^(-1)** (dot[p, \[Gamma]] +mass)** OGferm@ ords[[j]], {j, m}]**
				\[CapitalDelta][mass]^(-1)** (dot[p, \[Gamma]]+ mass)
		,{ords, orders}]
	,{m, 0, ord/2}]
];


(* ::Text:: *)
(*Apply tilde operation on propagators according to whether a field is bosonic or fermionic*)


TildeExpand@expr_:=Module[{},
	expr/. {
		\[CapitalDelta]t[eta:Alternatives[\[CapitalPhi], \[Phi], cV, cA]@ i_, n_]:> \[CapitalDelta]tBos[M@ eta, n+2],
		\[CapitalDelta]t[eta:Alternatives[A, V]@ i_, n_]:> - \[CapitalDelta]tBos[M@ eta, n+2],
		\[CapitalDelta]t[eta:Alternatives[\[CapitalPsi], \[Psi]]@ i_, n_]:> \[CapitalDelta]tFerm[M@ eta, n+1],
		\[CapitalDelta]t[eta:Alternatives[\[CapitalPhi], \[Phi], cV, cA], n_]:> \[CapitalDelta]tBos[M@ eta, n+2],
		\[CapitalDelta]t[eta:Alternatives[A, V], n_]:> - \[CapitalDelta]tBos[M@ eta, n+2],
		\[CapitalDelta]t[eta:Alternatives[\[CapitalPsi], \[Psi]], n_]:> \[CapitalDelta]tFerm[M@ eta, n+1],
		Xt:>Xop
	}/.{M[(\[Phi]|\[Psi]|A|cA)@_]-> 0, M[(\[Phi]|\[Psi]|A|cA)]-> 0}(*set masses of light fields to zero*)
];


(* Building the Subscript[Overscript[G, ~], \[Mu]\[Nu]] *)
GtOrd[\[Mu]_, \[Nu]_, ord_/; ord >= 0]:= Module[{cofs, n},
	cofs= Unique@ Table[Symbol["\[Beta]"<> ToString@ n], {n, 1, ord}];
	(-I)^ord/((ord+2) ord!) G[li[\[Mu], \[Nu]], li@@ cofs]** NonCommutativeMultiply@@ Table[del@ li@ cofs[[n]], {n, ord}]
];
GtOrd[\[Mu]_, \[Nu]_, ord_/; ord < 0]= 0;

(* Build the Subscript[\[ScriptCapitalG], \[Eta]] for the tilde propagators Subscript[Overscript[\[CapitalDelta], ~], \[Eta]]=Subscript[\[CapitalDelta], \[Eta]]-Subscript[\[ScriptCapitalG], \[Eta]] *)
OGscal[ord_]:= Module[{\[Alpha], \[Beta], \[Gamma], n},
	-I AntiCommutator[p@ li@ \[Alpha], GtOrd[\[Alpha], \[Beta], ord]]** del@ li@ \[Beta] +
	Sum[NonCommutativeMultiply[GtOrd[\[Alpha], \[Beta], n], GtOrd[\[Alpha], \[Gamma], ord-2-n], del@ li@\[Beta], del@ li@\[Gamma]], {n, 0, ord- 2}]
];
OGferm[ord_]:= Module[{\[Alpha], \[Beta]}, -I \[Gamma]@ li@ \[Alpha]** GtOrd[\[Alpha], \[Beta], ord- 1]** del@ li@ \[Beta]];




(* ::Text:: *)
(*X operators: X[{\[Phi]1, \[Phi]2}, li[\[Mu],...], li[\[Nu],...] ] is interpreted as \!\(\*SubscriptBox[\(D\), \({\[Mu],  ... }\)]\) \!\(\*SubsuperscriptBox[\(X\), \(\[Phi]1\ \[Phi]2\), \(\[Nu] ... \)]\) *)


XopAux[\[Mu]_Symbol, 0]:= p@ li@ \[Mu];
XopAux[\[Mu]_Symbol, ord_Integer]:= Module[{\[Delta]}, I GtOrd[\[Mu], \[Delta], ord-2]** del@ li@ \[Delta]];

Xop[fields_, openDevs_, extraDevs_]:= Module[{expCofs, devCofs, ords, n, m, xord},
	expCofs= Unique@ Table[Symbol["\[Beta]"<> ToString@ n], {n, 1, extraDevs}];
	devCofs= Unique@ Table[Symbol["\[Rho]"<> ToString@ n], {n, 1, openDevs}];
	Sum[
		xord= First@ ords;
		(-I)^xord/ xord!  NonCommutativeMultiply[X[fields, li@@ expCofs[[;;xord]], li@@ devCofs[[;;openDevs]] ],
			NonCommutativeMultiply@@ Table[del@ li@ expCofs[[m]], {m, xord}],
			NonCommutativeMultiply@@ Thread@ XopAux[devCofs[[;;openDevs]], ords[[2;;]] ]
		]
	,{ords, IntegerSets[extraDevs, openDevs+ 1]} ]
];


(* ::Subsubsection::Closed:: *)
(*Expansion of  symmetrized derivatives*)


CD[\[Mu]_List, expr_]:= TakeDev[li@@ \[Mu], expr];
CD[\[Mu]_, expr_]:= TakeDev[li@ \[Mu], expr];


(* ::Text:: *)
(*CovD[li[\[Mu],\[Nu],...], O, li[\[Alpha], \[Beta]...]] =  (Subscript[D, \[Mu]] Subscript[D, \[Nu]]...)O^(\[Alpha]\[Beta]...)*)


DZeroQ[f_? DZeroQ[x__]]:=And@@ DZeroQ/@ {x};
DZeroQ[f_[x__]]:= False;
DZeroQ[_]= True;
DZeroQ[CovD]= False;


TakeDev[li[\[Mu]_, \[Nu]__], expr_]:= TakeDev[li@ \[Mu], TakeDev[li@ \[Nu], expr]];
TakeDev[li@ \[Mu]_, _? DZeroQ]= 0;
TakeDev[\[Mu]_li, expr:Alternatives[_Plus, _List]]:= TakeDev[\[Mu], #]&/@ expr;
TakeDev[li@ \[Mu]_, expr:Alternatives[_Times, _NonCommutativeMultiply]]:=
	Module[{n}, Sum[MapAt[TakeDev[li@ \[Mu], #]&, expr, n], {n, Length@ expr}] ];
TakeDev[\[Mu]_li, Power[expr_, n_]]:= n TakeDev[\[Mu], expr] Power[expr, n-1];
TakeDev[li@ \[Mu]_, CovD[li@ \[Nu]___, op_, \[Alpha]_li]]:= CovD[li[\[Mu], \[Nu]], op, \[Alpha]];
TakeDev[li[], expr_]:= expr;


TakeSymDev[li[], expr_]:= expr;
TakeSymDev[\[Mu]_li, expr:Alternatives[_Plus, _List]]:= TakeSymDev[\[Mu], #]&/@ expr;
TakeSymDev[\[Mu]_li, _? DZeroQ]= 0;
TakeSymDev[\[Mu]_li, a_? DZeroQ op_]:= a TakeSymDev[\[Mu], op];
TakeSymDev[\[Mu]_li, op_** x_DiracProduct]:= TakeSymDev[\[Mu], op]** x;
TakeSymDev[\[Mu]_li, x_DiracProduct** op_]:= x** TakeSymDev[\[Mu], op];


DuplicateListQ@ x_List:= MatchQ[Tally@ x, {}|{{_,2}..}];
SplitSymmetrizedDevs@ expr_:= Module[{temp, inds},
	temp= expr/. {G[\[Alpha]_li, \[Mu]_li]:> 1/ Length@ Permutations@ \[Mu] Sum[CovD[inds, G, \[Alpha]], {inds, Permutations@ \[Mu]}],
		X[fields_, \[Mu]_li, \[Nu]_li]:> 1/ Length@ Permutations@ \[Mu] Sum[CovD[inds, X@fields, \[Nu]], {inds, Permutations@\[Mu] }],
		TakeSymDev[\[Mu]_li, op_]:> 1/ Length@ Permutations@ \[Mu] Sum[TakeDev[inds, op], {inds, Permutations@ \[Mu]}]};
	temp//.{
		CovD[li[\[Mu]___, \[Alpha]_, \[Nu]___, \[Alpha]_, \[Beta]_, \[Rho]___], rest__]/; FreeQ[{\[Nu], \[Rho]}, \[Beta]]:> CovD[li[\[Mu], \[Alpha], \[Nu], \[Beta], \[Alpha], \[Rho]], rest] -
			I TakeDev[li[\[Mu], \[Alpha], \[Nu]], GAction[li[\[Alpha], \[Beta]], CovD[li@ \[Rho], rest]] ],
		CovD[li[\[Mu]___, \[Alpha]_, \[Beta]_, \[Nu]___, \[Alpha]_, \[Rho]___], rest__]/; DuplicateListQ@ {\[Rho]}:> CovD[li[\[Mu], \[Beta], \[Alpha], \[Nu], \[Alpha], \[Rho]], rest] -
			I TakeDev[li[\[Mu]], GAction[li[\[Alpha], \[Beta]], CovD[li[\[Nu], \[Alpha], \[Rho]], rest]] ],
		CovD[li[\[Mu]___, \[Alpha]_, \[Beta]_, \[Nu]___, \[Rho]___], op_, x:li@ OrderlessPatternSequence[\[Alpha]_,\[Sigma]___] ]/; (DuplicateListQ@ {\[Rho]} && DuplicateFreeQ@ {\[Mu], \[Beta], \[Nu]} && DuplicateFreeQ@ {\[Beta], \[Sigma]}):>
			CovD[li[\[Mu], \[Beta], \[Alpha], \[Nu], \[Rho]], op, x] -I TakeDev[li@ \[Mu], GAction[li[\[Alpha], \[Beta]], CovD[li[\[Nu], \[Rho]], op, x]]],
		NonCommutativeMultiply[a___, Gpat:CovD[\[Mu]_li, G| _G, li@ OrderlessPatternSequence[\[Alpha]_, \[Beta]_]], b___,
			CovD[li[\[Nu]___, \[Alpha]_, \[Beta]_, \[Rho]___], rest__], c___]:>
			-I/2 NonCommutativeMultiply[a, Gpat, b, TakeDev[li@ \[Nu], GAction[li[\[Alpha], \[Beta]], CovD[li@ \[Rho], rest]] ], c],
		NonCommutativeMultiply[a___, CovD[li[\[Nu]___, \[Alpha]_, \[Beta]_, \[Rho]___], rest__], b___,
			Gpat:CovD[\[Mu]_li, G| _G, li@ OrderlessPatternSequence[\[Alpha]_, \[Beta]_]], c___]:>
			-I/2 NonCommutativeMultiply[a, TakeDev[li@ \[Nu], GAction[li[\[Alpha], \[Beta]], CovD[li@ \[Rho], rest]] ], b, Gpat, c]
	}
];


GAction[\[Mu]_li, op: CovD[_, _X| G, _]]:= Commutator[CovD[li[], G, \[Mu]], op];
GAction[\[Mu]_li, CovD[\[Nu]_li, G@ inds_List, \[Rho]_li]]:= Module[{a,rep=inds[[1,2]]},
	CovD[li[], G@ {inds[[1]], Index[a,rep]}, \[Mu]]** CovD[\[Nu], G@ {Index[a,rep], inds[[2]]}, \[Rho]]-
		CovD[\[Nu], G@ {inds[[1]], Index[a,rep]}, \[Rho]]** CovD[li[], G@ {Index[a,rep], inds[[2]]}, \[Mu]]
];
GAction[\[Mu]_li, CovD[\[Nu]_li, Field[l_, type_, inds_List, charges_List], \[Rho]_li]]:= Module[{a, ind, c},
	Sum[c[[1]] CovD[li[], G@ c[[0]], \[Mu]]** CovD[\[Nu], Field[l, type, inds, charges], \[Rho]], {c, charges}] +
	Sum[
		If[MatchQ[ind[[2]], Alternatives@@$GlobalSymmetries], 
			0,
			CovD[li[], G@{ind, Index[a,ind[[2]]]}, \[Mu]]** CovD[\[Nu], Field[l, type, inds/. ind-> Index[a,ind[[2]]], charges], \[Rho]]
		]
	,{ind, inds}]
];
GAction[\[Mu]_li, CovD[\[Nu]_li, Bar@ Field[l_, type_, inds_List, charges_List], \[Rho]_li]]:= Module[{a, ind, c},
	- Sum[c[[1]] CovD[li[], G@ c[[0]], \[Mu]]** CovD[\[Nu], Bar@ Field[l, type, inds, charges], \[Rho]], {c, charges}] -
	Sum[
		If[MatchQ[ind[[2]], Alternatives@@$GlobalSymmetries], 
			0,
			CovD[li[], G@{Index[a,ind[[2]]], ind}, \[Mu]]** CovD[\[Nu], Bar@ Field[l, type, inds/. ind-> Index[a,ind[[2]]], charges], \[Rho]]
		]
	,{ind, inds}]
];
HoldPattern@ GAction[\[Mu]_li, CovD[\[Nu]_li, Transp@ f:Alternatives[_Field, Bar@ _Field], \[Rho]_li]]:=
	GAction[\[Mu], CovD[\[Nu], f, \[Rho]]]/. Field@ x__:> Transp@ Field@ x;


(* ::Subsubsection:: *)
(*Simplifying identities*)


(* ::Text:: *)
(*Operator type*)


FieldTally@ op_:= Sort@ Cases[(op/. Power[y_, n_Integer/; n> 0]:>
	NonCommutativeMultiply@@ConstantArray[y, n]), CovD[_, f:(_Field| _Bar| _Transp), _]:> f, Infinity,Heads->False];
DerivativeCount@ op_:= Total@ Cases[op/. Power[y_, n_Integer/; n> 0]:>
	NonCommutativeMultiply@@ConstantArray[y, n], CovD[\[Mu]_li, fs_, _]:> Length@ \[Mu] + If[MatchQ[fs, G| _G], 2, 0], Infinity];
OperatorType@ term_:= First@ Cases[term, LTerm[_, op_]:> {DerivativeCount@ op, FieldTally@ op},Infinity];


(* ::Text:: *)
(*For separating out the operator part*)


Operator[a_?NonOperatorQ op_]:=a Operator@ op;
Operator[ops_Plus]:=Operator/@ops;


(* ::Text:: *)
(*For mapping operators to vectors and back*)


OpToVec[op_, opsList_]:= Module[{replacementList, i},
	replacementList= Table[opsList[[i]]-> 
		UnitVector[Length@ opsList, i], {i, Length@ opsList}];
	op/. replacementList
];
VecToOp[vec_List, opsList_]:= vec . opsList;


(* ::Text:: *)
(*For sorting operators, to choose preferred basis*)


rankingReplacements={
	CovD[_, G|_G, _]:> .2,
	CovD[li[___, \[Mu]_], _, li@ OrderlessPatternSequence[\[Mu]_, ___]]:> .1,
	CovD[li[___, \[Mu]_, \[Mu]_], _, _]:> .1,
	CovD[li[\[Mu]__], _Transp| _Bar, _]:> -.1
	(*NonCommutativeMultiply[CovD[li[\[Mu]__], _Transp| _Bar, _], ___]:> -.1*)
};
OperatorRanking@ op_:= Module[{r},
	Sum[Plus@@ Cases[op, r, Infinity], {r, rankingReplacements}]
];


(* ::Text:: *)
(*Identities*)


gIdentities={
	(*For STr terms without substitutions *)
	NonCommutativeMultiply[a___, CovD[li@\[Mu]_, G, li[\[Nu]_, \[Rho]_]], b___, CovD[li@\[Mu]_, G, li[\[Nu]_, \[Rho]_]], c___]:>
		( 2 NonCommutativeMultiply[a, CovD[li@\[Mu], G, li[\[Rho], \[Nu]]], b, CovD[li@\[Rho], G, li[\[Mu], \[Nu]]], c]),
	NonCommutativeMultiply[a___, CovD[li[\[Mu]___, \[Alpha]_, \[Beta]_,\[Nu]___], op_, \[Rho]_li], b___]:>
		(NonCommutativeMultiply[a, CovD[li[\[Mu], \[Beta], \[Alpha], \[Nu]], op, \[Rho]], b]
		-I NonCommutativeMultiply[a, TakeDev[li@ \[Mu], GAction[li[\[Alpha], \[Beta]], CovD[li@ \[Nu], op, \[Rho]]]], b]),
	(*For STr terms with substitutions *)
	rest_ CovD[li[\[Alpha]___, \[Mu]_], g1_G, li[\[Nu]_, \[Rho]_]]:> 
		-(rest TakeDev[li@ \[Alpha], CovD[li@ \[Nu], g1, li[\[Rho], \[Mu]]] + CovD[li@ \[Rho], g1, li[\[Mu], \[Nu]]]]),
	rest_ NonCommutativeMultiply[a___, CovD[li[\[Mu]___, \[Alpha]_, \[Beta]_,\[Nu]___], op_, \[Rho]_li], b___] :> 
		(rest NonCommutativeMultiply[a, CovD[li[\[Mu], \[Beta], \[Alpha], \[Nu]], op, \[Rho]], b]
		-I rest NonCommutativeMultiply[a, TakeDev[li@ \[Mu], GAction[li[\[Alpha], \[Beta]], CovD[li@ \[Nu], op, \[Rho]]]], b]),
	rest_ CovD[li[\[Mu]___, \[Alpha]_, \[Beta]_,\[Nu]___], op_, \[Rho]_li]:> 
		(rest CovD[li[\[Mu], \[Beta], \[Alpha], \[Nu]], op, \[Rho]] -I rest TakeDev[li@ \[Mu], GAction[li[\[Alpha], \[Beta]], CovD[li@ \[Nu], op, \[Rho]]]])
};


IbPidentities={
	(*For STr terms without substitutions *)
	x: NonCommutativeMultiply[a___, CovD[li[\[Mu]_, \[Nu]___], f_, \[Rho]_li], b___]:>
		(-TakeDev[li@ \[Mu], NonCommutativeMultiply[a, CovD[li@ \[Nu], f, \[Rho]], b]] +x),
	(*For STr terms with substitutions *)
	rest_ x: NonCommutativeMultiply[a___, CovD[li[\[Mu]_, \[Nu]___], f_, \[Rho]_li], b___]:>
		(-TakeDev[li@ \[Mu], rest NonCommutativeMultiply[a, CovD[li@ \[Nu], f, \[Rho]], b]] +rest x),
	rest_ CovD[li[\[Mu]_, \[Nu]___], f_, \[Rho]_li]:>
		(-TakeDev[li@ \[Mu], rest] CovD[li@ \[Nu], f, \[Rho]])
};


(* ::Text:: *)
(*Make identities from list of operators*)


MakeIdentities@ ops_List:= Module[{op, identities, allOps, newOps, temp, lhs, rhs, 
	idList={}, id, inds1, inds2},
	identities= gIdentities~ Join~ IbPidentities;
	(*Apply identities to all opertors and the resulting operators*)
	allOps= RelabelDummyIndices/@ ops; newOps= allOps;
	While[Length@ newOps >0,
		temp= DeleteCases[Flatten@ Table[
			CanonizeIndices@ RelabelDummyIndices[ ReplaceList[op, identities] -op/.
				x: CovD[_, _G, _]-> Commutative@ x/. Commutative@ x_-> x]
		,{op, newOps}], 0];
		idList= idList~ Join~ temp;
		temp= DeleteDuplicates@ Cases[Operator/@ temp, x_Operator:> x[[1]], Infinity];
		newOps= Complement[temp, allOps];
		allOps= allOps~ Join~ newOps;
	];
	If[Length@ idList=== 0, Return@ {}; ];
	(*Here would be the time to sort the operator list*)
	allOps= SortBy[allOps, OperatorRanking];

	(*Map identities to vectors in the vector space of operators*)
	idList= DeleteCases[RowReduce@ OpToVec[idList, allOps], {0..}];

	(*Make replacement rules*)
	Table[
		temp= First@ FirstPosition[id, 1];
		lhs= allOps[[temp]]; 
		rhs= lhs- VecToOp[id, allOps];
		(*Identify indices and use these to make operator patterns*)
		inds1= Cases[RemovePower@ lhs, _Index, Infinity];
		inds2= Complement[Cases[RemovePower@ rhs, _Index, Infinity], inds1];
		inds2= Rule[#, Index[Unique@ #[[1]], #[[2]]] ]&/@ inds2;
		inds1= Thread@ Rule[inds1, MapAt[(Pattern[#, Blank[]] &), inds1, {;;,1}]];
		
		(*Make replacement with pattern for indices*)
		(lhs/. inds1) -> (rhs/. inds2)
	,{id, idList}]
];



(* ::Text:: *)
(*Simplify function*)


SimplifyOutput@ expr_:= Module[{operators, term, identities},
	operators= Expand@ expr;
	If[Head@ operators =!= Plus, Return@ operators; ];
	operators= GatherBy[List/@ List@@ operators, OperatorType]/.LTerm-> Times;
	
	CollectLTerms@ Sum[
		Catch[
			If[Length@ term === 1, Throw@ term[[1, 1]];]; 
			term= Plus@@ term[[;;, 1]];
			identities= MakeIdentities@ Cases[Operator@ term, x_Operator:> x[[1]], Infinity];
			(*Use unique indices to prevent overlap with indices in the identities*)
			RelabelDummyIndices[term, True]/. identities// RelabelDummyIndices
		]
	,{term, operators}]
];


(* ::Text:: *)
(*All kinds of simplifications*)


SuperSimplify[expr_] := RelabelLTermIndices@ RelabelLTermIndices@ CanonizeIndices@ RelabelDummyIndices@ ContractDelta[
	(SimplifyOutput@ CollectLTerms@ RefineDiracProducts@ CollectGammaMatrices@ expr)/.LTerm->Times]


(* ::Section:: *)
(*Main routines *)


(* ::Subsubsection:: *)
(*Supertrace expansion *)


(* ::Text:: *)
(*Check the arguments of the X[...] function:*)


STrTerm::xformmismatch= "Invalid form for the X list. The argument must be given as a List. Each X should have the form X[{f1, f2}], X[{f1, f2}, n], or X[{f1, f2}, {n1, n2,..}] for field types f1,f2 and integers n, n1,...";
STrTerm::fieldtypemismatch= "One or more of the field types supplied to the X's do not match \[CapitalPhi], \[CapitalPsi], \[Phi], \[Psi], or A.";
STrTerm::adjacentfieldmismatch= "The field types in adjacent X's do not match.";

HalfIntegerQ@ n_:=IntegerQ[2n];

(*Auxiliary function for checking X input*)
XInputCheck[Xs_]:= Module[{fieldList,i},
	(*invalid form of argument*)
	If[!MatchQ[Xs, {Alternatives[X[{_Symbol, _Symbol}], X[{_Symbol, _Symbol}, _? HalfIntegerQ | {_? HalfIntegerQ..}]].. }],
		Message[STrTerm::xformmismatch];
		Return@ False;
	];

	(*invalid fields*)
	fieldList= Flatten@ Xs[[;;, 1, ;;]];
	If[!MatchQ[fieldList, {(\[CapitalPhi]| \[CapitalPsi]| \[Phi]| \[Psi]| A| V| cV| cA)..}],
		Message[STrTerm::fieldtypemismatch];
		Return@ False;
	];

	(*not matching adjacent fields*)
	If[Nand@@ Table[fieldList[[2 i]] === fieldList[[Mod[2i +1, Length@ fieldList, 1] ]], {i, Length@ fieldList /2}],
		Message[STrTerm::adjacentfieldmismatch];
		Return@ False;
	];

	True
];


(* ::Text:: *)
(*Symmetry factor for the supertrace*)


STrSymmetryFactor[Xs_]:= Length@ DeleteDuplicates@ NestList[RotateRight, Xs, Length@Xs-1]/ Length@ Xs;


(* ::Text:: *)
(*Supertrace function for generic Xs *)


(* Check that the order in the EFT power counting is given properly *)
STrTerm::invalidorder= "Unkown order '`1`'. The order must be given either as n or {n} for some integer n.";
STrTerm[_, order: Except[_Integer| {_Integer}]]:= (Message[STrTerm::invalidorder, order]; Abort[];);

STrTerm[Xs_, order_Integer:6]:= Module[
	(*
		Xs    : is a list of the form Xs = {X[{\[CapitalPhi],\[Phi]}],X[{\[Phi],\[CapitalPsi]}],...}
		order : gives the order in the EFT power pounting up to which the terms should be calculated.
	*)
	{m},

	If[!XInputCheck@Xs,Abort[];];

	(*sum over all results up to the derisred order in the EFT power counting*)
	Sum[STrTerm[Xs,{m}],{m,order}]
];

STrTerm[Xs_, {order_Integer}]:= Module[
	(*
		Xs    : is a list of the form Xs = {X[{\[CapitalPhi],\[Phi]}], X[{\[Phi],\[CapitalPsi]}], ...}
		        More precisely:
		        STrTerm[ {X[ {<field 1>, <field 2>}, {<order with 0 open devs>, <order with 1 open devs>,...} ], <next X>, ...}, <#order of entire STr>].
		order : gives the order in the EFT power pounting at which the terms should be calculated.
	*)
	{expr, x, temp, devs, pos, inds, xOrders, openDevs, devCases, totalPropOrder, totalXOrder, a, preFact},

	(*check the input*)
	If[!XInputCheck@ Xs, Abort[];];

	(*define an index label for every X[...] in Xs*)
	inds= {"i", "j", "k", "l", "m", "n", "i2", "j2", "k2", "l2", "m2", "n2"}[[;;Length@ Xs]];

	(*Yields a list of the orders of the X's with different numbers of open derivatives*)
	xOrders= (Xs/. X[fields_]:> X[fields, Xords@ fields] /.
		X[fields_, n: Except@ _List]:> X[fields, {n}])[[;;, 2]];

	(*Cases for how many open derivatives at each X.*)
	devCases= Tuples[Range/@ Length/@ xOrders] -1;

	(*this is the sign from evaluatinig the gaussian path integral for bosons/fermions*)
	preFact= -I/2 STrSymmetryFactor@ Xs Switch[Xs[[1, 1, 1]],
		\[CapitalPhi] | \[Phi] | V  | A,  +1,
		\[CapitalPsi] | \[Psi] | cV | cA, -1
	];

	(*sum over every combination of numbers of open derivatives in the different X*)
	Sum[
		(*Putting the trace expression together with propagators of the relevant type*)
		totalXOrder= Sum[xOrders[[a, openDevs[[a]] +1]], {a, Length@ Xs}];

		(*Xt[fields, open derivatives, extraDevs from tilde expansion]*)
		pos= 0;
		expr= NonCommutativeMultiply@@ Table[pos++;
			\[CapitalDelta]t[x[[1, 1]]@ inds[[pos]], 0]** Xt[{x[[1, 1]]@ inds[[pos]], x[[1, 2]]@ inds[[Mod[pos +1, Length@ Xs, 1]]]}, openDevs[[pos]], 0]
			,{x, Xs}];

		(*Expanding the term up to the given number of derivatives*)
		expr= Sum[
			temp= expr;
			temp[[;;, -1]]+= devs;
			temp
		,{devs, IntegerSets[order -totalXOrder, Length@ expr]}];

		expr= TildeExpand@ expr;

		(*Performing the momentum integration and various simplifications*)
		expr= expr// PerformMomDerivatives// CollectGammaMatrices// ExtractMomenta// RefineDiracProducts//
			CollectPropagators// LorentzSimplify// CanonizeIndices;
		expr= preFact expr// MultiScaleIntegrate// EpsExpand// Expand// LorentzSimplify//
			SplitSymmetrizedDevs// CanonizeIndices// Expand;
		CollectLTerms@expr/. {Log[a_] -Log[b_]-> Log[a/b], n_ Log[a_] +m_ Log[b_]/; m === -n-> n Log[a/b]}
	,{openDevs, devCases}]
]


(* ::Subsubsection::Closed:: *)
(*Log term*)


LogTerm::invalidorder= "Unkown order '`1`'. The order must be given either as n or {n} for some positive integer n.";
LogTerm::notfield= "Invalid field type. The first argument of LogTerm must be \[CapitalPhi], \[CapitalPsi], V, or c.";


HeavyFieldCheck@ _:= (Message[LogTerm::notfield]; False);
HeavyFieldCheck[\[CapitalPhi]| \[CapitalPsi]| V| cV]:= True;


LogTerm[_, order:Except[_Integer?Positive| {_Integer?Positive}]]:= (Message[LogTerm::invalidorder, order]; Abort[];);
LogTerm[field_, order_Integer: 6]:= Module[{m},
	If[!HeavyFieldCheck@ field, Abort[]; ];
		Sum[LogTerm[field, {m}], {m, order}]
	];


LogTerm[field_, {order_Integer}]:= Module[{expr, factor},
	If[!HeavyFieldCheck@ field, Abort[]; ];
	(*Overall factor determined by boson/fermion, \[Xi] integral, and Lorentz trace.*)
	factor= Switch[field,
		\[CapitalPhi], 2 M[field@ "i"]^2,
		\[CapitalPsi], -M[field@ "i"],
		V, -2 (4-2\[Epsilon]) M[field@ "i"]^2,
		cV, -2 M[field@ "i"]^2
	]/ (order-4+2\[Epsilon]);

	expr= TildeExpand@ \[CapitalDelta]t[field@ "i", order]// PerformMomDerivatives// CollectGammaMatrices //ExtractMomenta;
	If[field === \[CapitalPsi],
		expr= expr// CollectGammaMatrices// DiracTrace;
	];
	expr= expr// CollectPropagators// LorentzSimplify// CanonizeIndices;
	expr= I/2 factor expr// LogTermIntegrate// EpsExpand// Expand//
		LorentzSimplify// SplitSymmetrizedDevs// CanonizeIndices //Expand;

	expr// CollectLTerms// SimplifyOutput
];


(* ::Subsubsection:: *)
(*Power terms*)


PowerTerms::invalidXList= "Invalid form for the X list. The argument must be given as a List. Each X should have the form X[{f1, f2}], X[{f1, f2}, n], or X[{f1, f2}, {n1, n2,..}] for field types f1,f2 and integers n, n1,...";
PowerTerms::invalidOptionalArg= "Invalid argument '`1`'. This argument must be either as n or {n} for some positive integer n.";
PowerTerms::fieldtypemismatch= "One or more of the field types supplied to the X's do not match \[CapitalPhi], \[CapitalPsi], \[Phi], \[Psi], or A.";


PowerTerms[XList_,maxDim_:6,Xterms_:100]:=Module[{XListFull,XListSimp,XListOrd,ToXForm,Traces,SymFact},

	(*Checks the input*)
	If[!MatchQ[maxDim, (_Integer? Positive )|{_Integer? Positive}], Message[PowerTerms::invalidOptionalArg, maxDim]; Abort[];];
	If[!MatchQ[Xterms, (_Integer? Positive )|{_Integer? Positive}], Message[PowerTerms::invalidOptionalArg, Xterms]; Abort[];];

	(*invalid form of argument*)
	If[!MatchQ[XList, {Alternatives[X[{_Symbol, _Symbol}], X[{_Symbol, _Symbol}, _? HalfIntegerQ | {_? HalfIntegerQ..}]].. }],
		Message[PowerTerms::invalidXList];
		Abort[];
	];
	
	If[!MatchQ[Flatten@ XList[[;;, 1, ;;]], {(\[CapitalPhi]| \[CapitalPsi]| \[Phi]| \[Psi]| A| V| cV| cA)..}],
		Message[PowerTerms::fieldtypemismatch];
		Abort[];
	];
	
	(* In case the user did not do it, add hermitian conjugate interactions *)
	XListFull=Join[XList,XList/.{X[fields_]:>X[fields//Reverse],X[fields_,ord_]:>X[fields//Reverse,ord]}]//DeleteDuplicates;

	(* Take out the order of the interactions and store them in XListOrd *)
	XListSimp=XListFull/.X[fields_,ord_]:>X[fields];
	XListOrd=DeleteCases[XListFull/.{X[fields_,ord_]:>X[fields]->X[fields,ord],X[fields_]:>0},0];

	(* Convert tuples of fields into lists of X terms for STr *)
	ToXForm[InputList_]:=Module[{k,tmplist,tmpFields},
		tmplist={};
		For[k=1,k<=Length[InputList],k++,
			If[k!=Length[InputList],
				tmpFields={InputList[[k]],InputList[[k+1]]},
				tmpFields={InputList[[k]],InputList[[1]]}
			];
			AppendTo[tmplist,X[tmpFields]]
		];
		tmplist
	];

	(* This function creates all PowerTerms with n X insertions *)
	Traces[n_]:=Module[{HeavyFieldsList,Fields,TrList},
		HeavyFieldsList={\[CapitalPsi],\[CapitalPhi],V,cV};
		(* Collect the fields present in XListSimp *)
		Fields=Map[#[[1]] &,XListSimp]//Flatten//DeleteDuplicates;

		(* Create all possible tuples of n fields in Fields, filter so it always starts with a heavy field, remove cyclic permutations, and convert into X terms *)
		TrList=Select[ToXForm/@DeleteDuplicatesBy[Select[Tuples[Fields,n],MemberQ[HeavyFieldsList,#[[1]]]&],(First@
    Sort@NestList[RotateLeft, #, Length@# - 1] &)],SubsetQ[XListSimp,#]&];

		(* Add order and filter to the given dimension in maxDim *)
		If[NumberQ[maxDim],
			TrList=Select[TrList/.XListOrd/.X[fields_]:>X[fields,fields/.Xords],(# /.X[fields_,ord_]:>Min[ord]//Total)<= maxDim &];
			,
			TrList=Select[TrList/.XListOrd/.X[fields_]:>X[fields,fields/.Xords],(# /.X[fields_,ord_]:>Min[ord]//Total)==(maxDim[[1]]) &];
		];

		Plus@@STr/@TrList
	];

	(* Determine whether Xterms is fixed order (with {}) or inclusive (without {}) and give the corresponding output *)
	Xnum= Min[If[NumberQ@ maxDim, maxDim, First@ maxDim], If[NumberQ@ Xterms, Xterms, First@ Xterms]];
	If[NumberQ[Xterms],
		Res=0;
		Module[{i},For[i=1,i<=Xnum,i++,Res+=Traces[i]]];
		Res,
		Traces[Xnum]
	]
]

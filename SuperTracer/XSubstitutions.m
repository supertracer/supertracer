(* ::Package:: *)

Package["SuperTracer`"]


(* ::Title:: *)
(*SuperTracer`XSubstitutions`*)


(* ::Subtitle:: *)
(*This file contains the functions required for performing the XSubstitutions.*)


(* ::Chapter:: *)
(*Public:*)


(* ::Section:: *)
(*Scoping:*)


(* ::Subsubsection:: *)
(*Exported*)


PackageExport["AddField"]
PackageExport["RemoveField"]
PackageExport["Field"]


PackageExport["Index"]
PackageExport["Flavor"]


PackageExport["GA"]
PackageExport["GnA"]


PackageExport["\[Delta]"]
PackageExport["eps"]
PackageExport["T"]


PackageExport["Dim"]
PackageExport["S2R"]
PackageExport["C2R"]


PackageExport["AddGlobalSym"]
PackageExport["ResetGlobalSym"]


PackageExport["RedefineIndexLabels"]


PackageExport["ShowRep"]


(* ::Subsubsection:: *)
(*Internal*)


PackageScope["RelabelDummyIndices"]


PackageScope["RelabelLTermIndices"]


PackageScope["GetFields"]


PackageScope["CloseFermionTrace"]


PackageScope["CanonizeSpinorLines"]


PackageScope["ReformatDummyIndices"]
PackageScope["FindDummyIndices"]


PackageScope["XSubPattern"]


PackageScope["ContractDelta"]


PackageScope["RemovePower"]


PackageScope["$GlobalSymmetries"]


(* ::Section:: *)
(*Usage Messages*)


AddField::usage=
"AddField[label, type,<charges>] defines a new field of the given type and label so it can be used in an X substitution in the form lable[indices], where indices is a list of symbols representing indices.
The argument <charge> is optional and should be either clabel[charge] in the case of a single U(1) charge, where clabel is a label for the U(1) symmetry chosen by the user and charge is a number specifying the charge, or <charge> is given by {clabel1[charge1],clabel2[charge2],...} if the field is charged under multiple U(1) groups.";


RemoveField::usage=
"RemoveField[alias] removes the field with the label alias from the list of defined fields.";


Field::usage=
"Field[label,type,indices,charges] is used as an object that represents a field inside a covariant derivative with the given label, type, indices and charges.";


Index::usage=
"Index[expression] is a header to distinguish internally that expression is an index variable.";


Flavor::usage=
"Flavor is a symbol used to specifies that some index is a flavor index.";


GA::usage=
"GA[{\[Mu],\[Nu]},tag] denotes the field-strength tensor for Abelian groups. The arguments \[Mu] and \[Nu] are Lorentz indices and tag is the symbol used to label the charge of the Abelian symmetry.";


GnA::usage=
"GnA[{\[Mu],\[Nu]},{a,b}] denotes the field-strength tensor for non-Abelian groups. The arguments \[Mu] and \[Nu] are Lorentz indices and a and b are non-Abelian group indices.";


\[Delta]::usage=
"\[Delta][{a,b}] is the delta function \!\(\*SubscriptBox[\(\[Delta]\), \(ab\)]\).";


eps::usage=
"eps[{a, b}] is the generic 2-index antisymmetric tensor satisfying \!\(\*SubscriptBox[\(eps\), \(ab\)]\) \!\(\*SubscriptBox[\(eps\), \(bc\)]\)= -\!\(\*SubscriptBox[\(\[Delta]\), \(ac\)]\)."


T::usage=
"T[{A, a, b}] is a group generator \!\(\*SuperscriptBox[\(T\), \(Aab\)]\)."


Dim::usage=
"Dim[rep] denotes the dimension of the representation rep.";


S2R::usage=
"S2R[rep] denotes the Dynkin index of the representation rep.";


C2R::usage=
"C2R[rep] denotes the Dynkin index of the representation rep.";


AddGlobalSym::usage=
"AddGlobalSym[sym] adds the symmetry denoted by sym to the list of global symmetries.";

ResetGlobalSym::usage=
"ResetGlobalSym[sym] resets the list of global symmetries to only include the group Flavor.";


RedefineIndexLabels::usage=
"RedefineIndexLabels[stringlist] changes the symbols used for displaying indices. The argument stringlist must be a list of strings and defines the labels that should be used for printing indices.";


ShowRep::usage="ShowRep[True|False] sets a tag whether to print the representation of indices as subscripts or to omit them. By default representations are not displayed.";


(* ::Text:: *)
(*--- Internal ---*)


RelabelDummyIndices::usage=
"RelabelDummyIndices[expr] relabels all dummy indices for every term appearing in expr in a canonical way.";


RelabelDummyIndicesInOneTerm::usage=
"RelabelDummyIndicesInOneTerm[expr,bool] relabels all dummy indices in the single term expr. The argument bool is optional (if given it must be True|False) and specifies whether unique or canonical labels should be given to the dummy indices.";


ReformatDummyIndices::usage=
"ReformatDummyIndices[expr] finds all instances of Index[a_] in every term of the expression expr separately and then replaces a->Index[a].";


FindDummyIndices::usage=
"FindDummyIndices[expr] returns a list of all repeated indices in expr, where expr must be a single Term.";


ContractDelta::usage=
"ContractDelta[expression] contracts the indices of all \[Delta] in expression.";


CanonizeSpinorLines::usage=
"CanonizeSpinorLines[expr] removes non-spinors from NonCommutativeMultiply.";


(* ::Section:: *)
(*Formatting*)


(* ::Subsection:: *)
(*Index Formatting*)


RedefineIndexLabels[l_List,n_:100]:=Module[{list=DeleteDuplicates@Map[ToString,l]},
	$IndexAlphabet=list;
	$IndexAlphabetAssoc=BuildIndexAssoc[list,n];
];


(* ::Subsubsection::Closed:: *)
(*Labels to be used for displaying indices*)


$IndexAlphabet={"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"};


(* ::Subsubsection:: *)
(*Build up the necessary Associations*)


BuildIndexAssoc[alph_List,n_:100]:=Association@Table[
	Module[{auxx=QuotientRemainder[c-1,Length@alph],aux,auxR},
		aux=auxx[[1]];
		auxR=auxx[[2]];
		If[aux==0,
			ToExpression["Global`d$$"<>ToString[c]]->alph[[c]],
			ToExpression["Global`d$$"<>ToString[c]]->Subscript[ToString@alph[[auxR+1]],ToString[aux]]
		]
	],
	{c,n}
]


$IndexAlphabetAssoc=BuildIndexAssoc[$IndexAlphabet]


(* ::Subsubsection:: *)
(*The actual formatting of Index*)


(*Format[Flavor@ x_, StandardForm]:= x;*)


ShowRep[bool_?BooleanQ]:=($PrintIndexRepresentations = bool;)


$PrintIndexRepresentations = False;


Index/:MakeBoxes[Index[label_,rep_],StandardForm]:=If[$PrintIndexRepresentations,
	If[MemberQ[Keys@$IndexAlphabetAssoc,label],
		SubscriptBox[$IndexAlphabetAssoc@label,ToString@rep],
		SubscriptBox[ToString@label,ToString@rep]
	]
	,
	If[MemberQ[Keys@$IndexAlphabetAssoc,label],
		ToBoxes[$IndexAlphabetAssoc@label],
		ToBoxes[ToString@label]
	]
]




(* ::Subsubsection::Closed:: *)
(*Formatting of expressions that have indices as arguments*)


MakeBoxes[f_[{}], StandardForm]:= MakeBoxes@ f;
MakeBoxes[f_[indlist:{_Index..}],StandardForm]:=SuperscriptBox[
	MakeBoxes[f],
	RowBox@Map[MakeBoxes[#,StandardForm]&,indlist]
]


MakeBoxes[f_[A___, {}, B___], StandardForm]:= MakeBoxes@ f[A, B];
MakeBoxes[f_[A___,indlist:{_Index..},B___],StandardForm]:=SuperscriptBox[
	MakeBoxes[f[A,B]],
	RowBox@Map[MakeBoxes[#,StandardForm]&,indlist]
]


MakeBoxes[Bar@f_[A___], StandardForm]:= MakeBoxes@ Bar[f][A];


(*MakeBoxes[f_[indlist_List],StandardForm]/;(And@@((Head[#]===Index)&/@indlist)):=SubscriptBox[
	MakeBoxes[f],
	RowBox@Map[MakeBoxes[#,StandardForm]&,indlist]
]*)


(*MakeBoxes[f_[A___,indlist_List,B___],StandardForm]/;(And@@((Head[#]===Index)&/@indlist)):=SubscriptBox[
	MakeBoxes[f[A,B]],
	RowBox@Map[MakeBoxes[#,StandardForm]&,indlist]
]*)


(* ::Subsection::Closed:: *)
(*Other symbols*)


Field/: MakeBoxes[Field[l_, \[Phi]|\[CapitalPhi]|V|\[Psi]|\[CapitalPsi], _], StandardForm]:= ToBoxes@ l;


Format[Dim[rep_],StandardForm]:=Subscript["N",ToString@rep]
Format[S2R[rep_],StandardForm]:=Row@{Subscript["S",2], "[", ToString@rep, "]"};
Format[C2R[rep_],StandardForm]:=Row@{Subscript["C",2], "[", ToString@rep, "]"};


Format[eps@{ind1:Index[_,rep_], ind2:Index[_,rep_]}, StandardForm]:=
	Subsuperscript["\[Epsilon]", ToString[rep],Row@{ind1,ind2}];


Format[T@{indA_Index, ind1:Index[_,rep_], ind2:Index[_,rep_]}, StandardForm]:=
	Subsuperscript["T", ToString[rep],Row@{indA,ind1,ind2}];


(* ::Chapter:: *)
(*Private:*)


(* ::Subsection:: *)
(*Shorthand*)


GA::invalidformat= "GA must be called as GA[{\[Mu], \[Nu]}, tag]."


GA[__]:= (Message[GA::invalidformat]; Abort[]);


GA[{\[Mu]_, \[Nu]_}, tag_Symbol]:= CovD[li[], G@ tag, li[\[Mu], \[Nu]]];


GnA::wrongrep= "The indices 'Index[`1`,`2`]' and 'Index[`3`,`4`]' do not belong to the same representation.";
GnA::invalidformat= "GnA must be called as GnA[{\[Mu], \[Nu]}, {rep@ a, rep@ b}]."


GnA[_, {rep1_@ a_, rep2_@ b_}]/; rep1 =!= rep2:= (Message[GnA::wrongrep, a, rep1, b, rep2]; Abort[]);
GnA[__]:= (Message[GnA::invalidformat]; Abort[]);


GnA[{\[Mu]_, \[Nu]_}, {rep1_[name1_], rep1_[name2_]}]:= CovD[li[], G@ {Index[name1,rep1], Index[name2,rep1]}, li[\[Mu], \[Nu]]];


(* ::Section::Closed:: *)
(*Field definitions*)


$FieldList=<||>;
GetFields[]:=Return@ (*Keys@*) $FieldList;


(* error messages *)
AddField::ErrorSymbolAlreadyUsed="The Symbol '`1`' is already used in some loaded context or already has some definitions. Please use another symbol.";

AddField::noli="A vector field must have at least one index, that is its Lorentz index.";

Index::format="The index '`1`' is not in the correct format. Indices must be given as GroupLabel[IndexName].";


(* Defines a new Field *)
AddField[label_Symbol, type:(\[Phi]|\[CapitalPhi]|\[Psi]|\[CapitalPsi]|V), charges_:{}, countingDim_:Automatic]:= Module[{chargeList},
	(*Check that the label symbol is not yet in use.*)
	If[Head[label::usage]=!=MessageName,
		Message[AddField::ErrorSymbolAlreadyUsed,label];
		Abort[];
	];
	
	chargeList= If[Head@ charges === List, charges, List@ charges];
	
	(* Add field to the list of fields *)
	AppendTo[$FieldList,label->
		<|Type-> type, Charge-> chargeList, Dimension-> countingDim|>
	];
	
	(*Set counting dimension of the field*)
	If[countingDim =!= Automatic,
		OpDim[Field[label, type, __]]:= countingDim;
	];

	(* Create the usage message for the new field depending on whether it is a vector or not. *)
	If[type === V,
		label::usage=(ToString[label] <> "[<indices>]: Gives a field with label " <> ToString[label] <> " of type " <> ToString[type]<>  " with the charges " <> ToString[charges] <> ". The argument <indices> is a list of symbols that specify all indices carried by the field. The first index in the list must be its Lorentz index, potetnially followed by a sequence of non-Abelian indices, which must be specified as GroupLabel[IndexName]."),
		label::usage=(ToString[label] <> "[<indices>]: Gives a field with label " <> ToString[label] <> " of type " <> ToString[type]<>  " with the charges " <> ToString[charges] <> ". The argument <indices> is an optional list of symbols that specify all non-abelian indices carried by the field. Each index must be specified as GroupLabel[IndexName].")
	];

	(* Define the alias for the new field *)
	label[]:= label@ {};
	label[indices_List]:=Module[
		{
			l = label,
			t = type,
			ind = indices,
			lorentzInd = li[],
			n
		},

		(*For vectors only: extract the Lorentz index at position 1 in indices.*)
		If[type === V,
			(*check that there is at least one index*)
			lorentzInd = Check[
				li@ First@ ind,
				Message[AddField::noli]; Abort[],
				{First::nofirst}
			];
			ind = ind[[2;;]]
		];

		(*apply internal index format*)
		ind = Table[
			(*Check the format of the indices*)
			If[!MatchQ[a,_Symbol[_Symbol|_Pattern|_Blank]],
				Message[Index::format,a];
				Abort[]
			];
			Index[Quiet[a[[1]]],a[[0]]]
			,
			{a,ind}
		];

		(*return*)
		CovD[li[], Field[l, t, ind, chargeList], lorentzInd]
	];

];


(* ::Subsubsection::Closed:: *)
(*Remove an already defined fields*)


RemoveField[alias_]:=(KeyDropFrom[$FieldList, alias]; Remove@alias)


(* ::Section:: *)
(*Indices*)


(* ::Subsection::Closed:: *)
(*General definitions*)


(* ::Text:: *)
(*Make Index Flat and OneIdentity*)


SetAttributes[Index,{Flat,OneIdentity}]


(*Index@ Flavor@ x_:= Flavor@ Index@ x;*)


(* Auxiliary function to remove powers in intermediate steps *)
RemovePower[input_]:=Module[
	{
		rule={Power[x___,n_Integer]:>Module[{Prod},Hold[Evaluate[Prod@@Table[x,{ii,n}]]]/.Prod->Times]}
	},

	(*The returned value needs a ReleaseHold[] afterwards.*)
	Return[Expand[input]/.rule]
]


(* ::Subsection:: *)
(*Bring (dummy) indices to internal format*)


ReformatDummyIndices[expr_]:=Module[
	{
		temp = Expand[expr],
		term
	},
	(*treat every term in a sum separately*)
	If[Head[temp]===Plus,
		temp = List@@temp,
		temp = List@temp
	];
	(*sum over all terms*)
	Sum[
		(*reformat the indices in every term*)
		term/.Cases[term, Index[label_,rep_]:>(rep@label->Index[label,rep]), All],
		{term,temp}
	]
]


(* ::Subsection::Closed:: *)
(*Find the dummy indices*)


FindDummyIndices::error1="Head of argument is Plus. FindDummyIndices cannot be called on a sum, but only on a single term.";
FindDummyIndices::error2="An index is apperaing more than twice.";


(* Function to find all dummy indices in a single term *)
FindDummyIndices[expression_]:=Module[
	{
		list={},
		pow,
		expr=Expand[expression/.LTerm->Times]
	},

	(* Throw error when expr is a sum of terms*)
	If[Head[expr]===Plus,
		Message[FindDummyIndices::error1];
		Abort[]
	];

	(* Temporarly remove all powers, here we do not need a release hold afterwards *)
	expr=RemovePower[expr];

	(* list all indices and their multiplicity found in expr *)
	list = Tally[Cases[expr,Index[_,_],All]];

	(* throw error if there is a tripple index *)
	If[Or@@Map[(#>2)&,(list/.List[a_,b_Integer]:>b)],
		Message[FindDummyIndices::error2];
		Abort[]
	];

	(* get all indices appearing twice *)
	Return[Cases[list,{x_,2}:>x]]
]


(* ::Subsection::Closed:: *)
(*Relabel the dummy indices in one single term*)


RelabelDummyIndicesInOneTerm[expr_,unique_:False]:=Module[
	{
		indexlist={},
		rule={},
		result
	},

	(* Unique | canonical dummy index labels*)
	If[unique,
		(*True: unique dummy indices are required:*)
		Module[{},
			result = expr;
			(*Find dummy indices*)
			indexlist = FindDummyIndices[result];

			(*Find replacement rules*)
			rule = Cases[
				indexlist,
				Index[label_,rep_]:>(Index[label,rep]->Index[Unique["u"],rep]),
				All
			];
		],

		(*False: canonically labled dummy indices are required:*)
		Module[{n=1},
			(*make dummy indices unique*)
			result = RelabelDummyIndicesInOneTerm[expr,True];
			(*Find the new dummy indices*)
			indexlist = FindDummyIndices[result];

			(*Find replacement rules*)
			rule = Cases[
				indexlist,
				Index[label_,rep_]:>(Index[label,rep]->Index[ToExpression["d$$"<>ToString[n++]],rep]),
				All
			];
		]
	];

	(*apply rules*)
	Return[result/.rule]
]


(* ::Subsection::Closed:: *)
(*Relabel the dummy indices in a sum of terms*)


(* When relabeling dummy indices in a list or matrix*)
RelabelDummyIndices[l_List, unique_:False]:= RelabelDummyIndices[#, unique]&/@l;


(* Function that replaces all dummy indices in a consitent manner *)
RelabelDummyIndices[expression_,unique_:False]:=Module[
	{
		expr=Expand[expression/.LTerm->Times],
		solution
	},

	(*treat every term in a sum separately*)
	If[Head[expr]===Plus,
		expr = List@@expr,
		expr = List@expr
	];

	(*relabel dummy indices in each term separately and sum them up*)
	solution = Sum[
		RelabelDummyIndicesInOneTerm[term,unique],
		{term,expr}
	];
	Return[solution]
]


(* ::Subsection::Closed:: *)
(*Relabel dummy indices LTerm by operators*)


(* ::Text:: *)
(*Relabel the dummy indices in LTerm according to the operator.*)


RelabelLTermIndices::openIndex="An index is not properly contracted. Cannot simplify LTerm with open indices.";


RelabelLTermIndices[arg_]:=Module[
	{
		expr = CollectLTerms@arg
	},
	expr = expr/.x_LTerm :> RelabelIndicesByOperator@ ExpandLTerm@ x;

	CollectLTerms@ expr
]


(*Expands the coefficient of the LTerm*)
ExpandLTerm[LTerm[a_,b_]]:=Module[{temp=Expand@a},
	If[Head@temp === Plus,
		temp = List@@temp,
		temp = List@temp
	];
	Plus@@Map[AuxLTerm[#,b]&,temp]
]


RelabelIndicesByOperator[x_Plus]:=Map[RelabelIndicesByOperator,x]


RelabelIndicesByOperator[x:Except[_AuxLTerm|_Plus|_ExpandLTerm]]:=x


RelabelIndicesByOperator[x:AuxLTerm[a_,b_]]:=Module[
	{
		unique,
		canonical,
		coef,
		op,
		coefInd,
		opInd,
		otherInd,
		allInd,
		counter=1
	},
	(*make indices unique*)
	unique = Table[
		c->Index[Unique[],c[[2]]],
		{c, DeleteDuplicates@ Cases[x,Index[_,_],All]}
	];
	coef = a/.unique;
	op = b/.unique;

	(*find the indices in coefficient and operator*)
	coefInd = Cases[coef, Index[_,_], All];
	opInd = Cases[op, Index[_,_], All];

	(*check that there are only dummy indices*)
	Cases[
		Tally[Join[coefInd,opInd]],
		{ii_,Except[2]}:>(Message[RelabelLTermIndices::openIndex,ii];Abort[]),
		1
	];

	(*get a single instance of every index in operator*)
	opInd = DeleteDuplicates[opInd];

	(*get one instance of every index that only appears in the coefficient*)
	otherInd = DeleteDuplicates@ Complement[coefInd,opInd];

	(*find all indices in the correct order*)
	allInd = Join[opInd,otherInd];
	canonical = Table[
		ind->Index[ToExpression["d$$"<>ToString[counter++]],ind[[2]]]
		,
		{ind, allInd}
	];

	LTerm[coef,op]/.canonical
]


(* ::Subsection::Closed:: *)
(*Contract \[Delta]/eps*)


(* ::Subsubsection::Closed:: *)
(*\[Delta] properties*)


\[Delta]::wrongrep="The indices 'Index[`1`,`2`]' and 'Index[`3`,`4`]' do not belong to the same representation.";


\[Delta][{Index[a_,rep1_],Index[b_,rep2_]}]/;rep1=!=rep2 := (Message[\[Delta]::wrongrep,a,rep1,b,rep2];Abort[])


\[Delta][{rep_[label1_],rep_[label2_]}] := \[Delta][{Index[label1,rep],Index[label2,rep]}];


\[Delta][{rep1_[a_], rep2_[b_]}]/; rep1 =!= rep2:= (Message[\[Delta]::wrongrep, a, rep1, b, rep2]; Abort[]);


(* ::Text:: *)
(*Contract \[Delta]'s with each other*)


\[Delta]/:Times[\[Delta]@{OrderlessPatternSequence[a_Index,b_Index]},\[Delta]@{OrderlessPatternSequence[b_Index,c_Index]}] := \[Delta]@{a,c}


(* ::Text:: *)
(*treat Subscript[\[Delta], aa]*)


\[Delta][{Index[i_,rep_],Index[i_,rep_]}]:=Dim[rep]


\[Delta]/:Power[\[Delta][{Index[a_,rep_],Index[b_,rep_]}],2]:=Dim[rep]


(* ::Subsubsection::Closed:: *)
(*Eps properties*)


eps::wrongrep= "The indices 'Index[`1`,`2`]' and 'Index[`3`,`4`]' do not belong to the same representation.";


eps[{Index[a_,rep1_], Index[b_,rep2_]}]/; rep1 =!= rep2:= (Message[eps::wrongrep, a, rep1, b, rep2]; Abort[]);


eps[{rep_[label1_], rep_[label2_]}]:= eps[{Index[label1, rep], Index[label2, rep]}];


eps[{rep1_[a_], rep2_[b_]}]/; rep1 =!= rep2:= (Message[eps::wrongrep, a, rep1, b, rep2]; Abort[]);


(* ::Text:: *)
(*Contract eps's with each other*)


eps/: Times[eps@{a_Index, b_Index}, eps@{b_Index, c_Index}]:= -\[Delta]@{a, c};
eps/: Times[eps@{a_Index, b_Index}, eps@{c_Index, b_Index}]:= \[Delta]@{a, c};
eps/: Times[eps@{b_Index, a_Index}, eps@{b_Index, c_Index}]:= \[Delta]@{a, c};
eps/: Power[eps@ {Index[a_, rep_], Index[b_,rep_]}, 2]:= Dim[rep];


eps@ {a_Index, a_Index}:= 0;


(* ::Text:: *)
(*Contract eps with \[Delta] *)


eps/: \[Delta]@{OrderlessPatternSequence[a_Index, x_Index]} eps@ {b___, x_Index, c___}:=
	eps@ {b, a, c};


(* ::Text:: *)
(*Contract eps with G*)


eps/: eps[x:{ OrderlessPatternSequence[a_Index, b_Index]}] * CovD[\[Mu]_li, G@ {b_Index, c_Index}, \[Nu]_li] *
	eps[y:{ OrderlessPatternSequence[c_Index, d_Index]}]:=
	Times@@ Signature/@ {x, {a, b}, y, {c, d}} CovD[\[Mu], G@ {d, a}, \[Nu]];


(* ::Subsubsection::Closed:: *)
(*Generators*)


(* ::Text:: *)
(*Input*)


T::wrongrep= "The indices 'Index[`1`,`2`]' and 'Index[`3`,`4`]' do not belong to the same representation.";


T[{_, rep1_@ a_, rep2_@ b_}]/; rep1 =!= rep2:= (Message[T::wrongrep, a, rep1, b, rep2]; Abort[]);


T@{repA_@A_, rep_@ a_, rep_@ b_}:= T@{Index[A, repA], Index[a, rep], Index[b, rep]};


(* ::Text:: *)
(*Bar action*)


Bar@ T@{A_, a_, b_}:= T@ {A, b, a};


(* ::Text:: *)
(*Contract with deltas*)


T@ {A_, a_, a_}:= 0;


T/: \[Delta]@{OrderlessPatternSequence[x_, y_]} T@{a___, y_, b___}:= T@ {a, x, b};


(* ::Text:: *)
(*Contract with eps*)


eps/: eps[x:{ OrderlessPatternSequence[a_Index, b_Index]}] * T@ {A_, b_, c_} *
	eps[y:{ OrderlessPatternSequence[c_Index, d_Index]}]:=
	Times@@ Signature/@ {x, {a, b}, y, {c, d}} T@ {A, d, a};


(* ::Text:: *)
(*Contract with generators*)


T/: T@{A_, a:Index[_,rep_], b_} T@{B_, b_, a_}:= \[Delta]@{A, B} S2R[rep];
T/: T@{A_, a_, b:Index[_,rep_]} T@{A_, b_, c_}:= \[Delta]@{a, c} C2R[rep];


(* ::Subsubsection::Closed:: *)
(*Contract the other \[Delta]'s*)


(* This function takes as argument a single term and contracts all the delta indices in this term *)
ContractDeltaInOneTerm[expr_]:=Module[
	{
		(*find all index pair that could be contracted*)
		indices = Cases[expr,\[Delta]@{OrderlessPatternSequence[a_Index,b_Index]}:>{a,b},All],
		noDelta = expr/.\[Delta]@{_,_}->1,
		result = expr,
		ind1,ind2
	},

	Do[
		ind1 = indexPair[[1]];
		ind2 = indexPair[[2]];

		If[!FreeQ[noDelta,ind1],
			result = (result /. \[Delta]@{OrderlessPatternSequence[ind1,ind2]}->1) /. ind1->ind2,
			If[!FreeQ[noDelta,ind2],
				result = (result /. \[Delta]@{OrderlessPatternSequence[ind1,ind2]}->1) /. ind2->ind1
			]
		],
		{indexPair,indices}
	];
	Return[result]
]


(* Function that takes any expression as argument and then contract the indices of all \[Delta] appearing. It also canonizes the indices in the return.*)
ContractDelta[arg_]:=Module[
	{
		expr = Expand@ arg,
		return
	},

	(*treat every term in a sum separately*)
	If[Head[expr]===Plus,
		expr = List@@expr,
		expr = List@expr
	];

	(*contract the \[Delta] in each term separately and sum them up*)
	return = Sum[
		ContractDeltaInOneTerm[term],
		{term,expr}
	];
	Return[RelabelDummyIndices@return]
]


(* ::Section:: *)
(*STrTerm with substitutions*)


(* ::Subsection:: *)
(*X function patterns *)


(* ::Text:: *)
(*Utility function for building the patterns for the substitutions*)


XSubPattern::duplicate="Duplicate detected: Gauge field cannot transform twice under the same representation.";


(* ::Subsubsection:: *)
(*X-substitution*)


XSubPattern@ l_List := XSubPattern/@ l;
XSubPattern[{f1_, f2_} -> operator_]:= Module[{count, lab},
	With[{op = ReformatDummyIndices[operator]/. (rep:Except[Global`\[Alpha], _Symbol])[label:(Global`i|Global`j)] :> Index[label,rep]},
		count = Sum[If[FreeQ[op, lab], 0, 1], {lab, {Global`\[Mu], Global`\[Nu]}}];
		Switch[count,
			0, X[{f1@ i$_, f2@ j$_}, \[Alpha]_li, li[]]:> (TakeSymDev[\[Alpha], UniqueLorentz@ RelabelDummyIndices[op,True]]/.{Global`i-> i$, Global`j-> j$}),
			1, X[{f1@ i$_, f2@ j$_}, \[Alpha]_li, li[\[Mu]$_]]:> (TakeSymDev[\[Alpha], UniqueLorentz@ RelabelDummyIndices[op,True]]/. {Global`i-> i$, Global`j-> j$, Global`\[Mu]-> \[Mu]$}),
			2, X[{f1@ i$_, f2@ j$_}, \[Alpha]_li, li[\[Mu]$_, \[Nu]$_]]:> (TakeSymDev[\[Alpha], UniqueLorentz@ RelabelDummyIndices[op,True]]/. {Global`i-> i$, Global`j-> j$, Global`\[Mu]-> \[Mu]$, Global`\[Nu]-> \[Nu]$})
		]
	]
];


(* ::Subsubsection::Closed:: *)
(*Mass-substitution*)


XSubPattern[M@f_ -> masses_List]:= Sequence[
	M@ f Power[\[CapitalDelta]@ M@ _f , n_]:> DiagonalMatrix[Power[\[CapitalDelta]/@ masses, n] masses],
	Power[M@ f, m_] Power[\[CapitalDelta]@ M@ _f , n_]:> DiagonalMatrix[Power[\[CapitalDelta]/@ masses, n] masses^m],
	Power[\[CapitalDelta]@ M@ f , n_]:> DiagonalMatrix[Power[\[CapitalDelta]/@ masses, n]],
	M@ f:> DiagonalMatrix@ masses,
	Power[M@ f, m_]:> DiagonalMatrix[masses^m]
];


(* ::Subsubsection::Closed:: *)
(*G-substitution*)


$GlobalSymmetries = {Flavor};


AddGlobalSym::repeatedsym="The symbol `1` is already defined as a global symmetry.";
AddGlobalSym::nosymmetry="The symbol `1` cannot be used as a global symmetry.";


AddGlobalSym[sym_]:=If[MemberQ[$GlobalSymmetries,sym],
	Message[AddGlobalSym::repeatedsym,sym],
	If[StringMatchQ[Context[sym],"SuperTracer`"] && sym=!=Flavor,
		Message[AddGlobalSym::nosymmetry,sym],
		AppendTo[$GlobalSymmetries,sym]
	]
]


ResetGlobalSym[]:=($GlobalSymmetries = {Flavor})


(*fieldReps is a list of lists for every field das belongs to the label f_*)
XSubPattern[G@f_ -> representations_List]:= With[{
		op = Table[CreateG[rep], {rep, representations}]
	},
	(*create the appropriate substitution patterns in matrix form*)
	If[MatchQ[f, A|V],
		CovD[li[], G@ {f@ i$_, f@ j$_}, \[Mu]$_li]:> 
			g[Global`\[Alpha]@ i$, Global`\[Alpha]@ j$] (DiagonalMatrix@ op/.{Global`i-> i$, Global`j-> j$, Global`\[Mu]-> \[Mu]$})
	,
		CovD[li[], G@ {f@ i$_, f@ j$_}, \[Mu]$_li]:> 
			(DiagonalMatrix@ op/.{Global`i-> i$, Global`j-> j$, Global`\[Mu]-> \[Mu]$})
	]
];


(* Create the bared version *)
(*CreateG[Bar@arg_List]:=(-1) * (CreateG[arg]/. CovD[li[],G[{a_,b_}],\[Mu]_]:>CovD[li[],G[{b,a}],\[Mu]])*)


CreateG[0|{0}|{}]:=0


(* Creates all G's acting on a single field *)
CreateG[arg_List]:=Module[
	{
		(*remove the Bar on charges and on global sym*)
		representations = arg/.(Bar@s_Symbol@(x_?NumberQ):>s[-x])/.(Bar[x:Alternatives@@$GlobalSymmetries]:>x),
		abelian,
		nonAbelian,
		global,
		local,
		deltaProduct,
		globalDeltaProduct
	},

	(*check that there are no duplicates in the representations*)
	If[!DuplicateFreeQ[representations],
		Message[XSubPattern::duplicate];
		Abort[]
	];

	(*divide into global, local, abelian, non-abelian representations*)
	global     = Cases[representations, Alternatives@@$GlobalSymmetries, All];
	local      = Complement[representations, global];
	abelian    = Cases[local,_Symbol@(_?NumberQ), All];
	nonAbelian = Complement[local, abelian];

	(*create the deltas for the flavor/global indices*)
	globalDeltaProduct = Times@@Table[
		\[Delta]@{Index[Global`i,delt], Index[Global`j,delt]}
		,
		{delt, global}
	];

	(*sum over all local representations*)
	Plus@@Table[
		(*create a \[Delta] for every non-abelian representation except for the current rep.*)
		deltaProduct = globalDeltaProduct * Times@@Table[
			\[Delta]@{Index[Global`i,delt], Index[Global`j,delt]}/.Bar[x_Symbol]:>x
			,
			{delt, DeleteCases[nonAbelian,rep]}
		];

		(*multiply the product of deltas with the G of the current rep.*)
		If[MemberQ[abelian,rep],
			rep[[1]] * deltaProduct * CovD[li[], G[rep[[0]]], Global`\[Mu]],
			If[MatchQ[rep,Bar@_Symbol],
				(*if rep is bared flip the indices and multiply with a minus*)
				(-1) * deltaProduct * CovD[li[], G[{Index[Global`j,rep[[1]]], Index[Global`i,rep[[1]]]}], Global`\[Mu]]
				,
				deltaProduct * CovD[li[], G[{Index[Global`i,rep], Index[Global`j,rep]}], Global`\[Mu]]
			]
		]
		,
		{rep, local}
	]
]


(* ::Subsection:: *)
(*Super trace with substitutions*)


(* ::Subsubsection::Closed:: *)
(*Input check*)


STrTerm::subsincomplete= "Substitution list is incomplete: it does not contain specifications for all `1`.";
STrTerm::scalmat= "X[`1`] must be substituted with a scalar or a matrix.";
STrTerm::mlist= "`1` must be substituted with a list of the diagonal masses.";
STrTerm::glist= "`1` must be substituted with a list of representaions, charges, and flavor indices for each field.";
STrTerm::subform= "The substitution must be a list with elements {f1, f2}->... and M[f1]->...";

SubstitutionCheck[Xs_, subs_]:= Module[{sub},
	If[!MatchQ[subs, {Rule[{_, _}|_M| _G, _]..}],
		Message[STrTerm::subform];
		Return@ False;
	];
	If[!SubsetQ[subs[[;;, 1]], Xs[[;;, 1]]],
		Message[STrTerm::subsincomplete, X];
		Return@ False;
	];
	If[!SubsetQ[Cases[subs, (g_G-> _):> First@ g], DeleteDuplicates@ Flatten@ Xs[[;;, 1]]],
		Message[STrTerm::subsincomplete, G];
		Return@ False;
	];
	Catch[Do[
		If[MatchQ[sub, fs_List-> rep_List/; !MatrixQ@ rep],
			Message[STrTerm::scalmat, sub[[1]] ];
			Throw@ False;
		];
		If[MatchQ[sub, _M-> ms_/; !VectorQ@ ms],
			Message[STrTerm::mlist, sub[[1]] ];
			Throw@ False;
		];

		If[MatchQ[sub, _G-> _]&& !MatchQ[sub, _G-> {(0|{0}|{}|Bar@_|{(_Symbol| _Symbol@_|_Flavor)..})..}],
			Message[STrTerm::glist, sub[[1]] ];
			Throw@ False;
		];
	,{sub, subs}];
	True]
];


(* ::Subsubsection:: *)
(*Function*)


STrTerm[_, order: Except[_Integer| {_Integer}| {_Integer, _Integer}], subs_]:= (Message[STrTerm::invalidorder, order]; Abort[];);


STrTerm[Xs_, order_Integer, subs_]:= Module[{m},
	If[!XInputCheck@ Xs, Abort[];];
	If[!SubstitutionCheck[Xs, subs], Abort[];];
	Sum[STrTerm[Xs, {m, order}, subs], {m, order}]
];


STrTerm[Xs_, {order_Integer}, subs_]:= STrTerm[Xs, {order, order}, subs];


STrTerm[Xs_, {order_Integer, cutOffDim_Integer}, subs_]:= Module[
	{expr, x, temp, devs, pos, xOrders, openDevs, devCases, totalPropOrder, totalXOrder, a, preFact},
	If[!XInputCheck@ Xs, Abort[];];
	If[!SubstitutionCheck[Xs, subs], Abort[];];

	(*Yields a list of the orders of the X's with different numbers of open derivatives*)
	xOrders= (Xs/. X[fields_]:> X[fields, Xords@ fields] /.
		X[fields_, n: Except@ _List]:> X[fields, {n}])[[;;, 2]];

	devCases= Tuples[Range/@ Length/@ xOrders] -1; (*Cases for how many open derivatives at each X.*)
	preFact= -I/2 STrSymmetryFactor@ Xs Switch[Xs[[1, 1, 1]],
		\[CapitalPhi] | \[Phi] | V  | A,  +1,
		\[CapitalPsi] | \[Psi] | cV | cA, -1
	];

	Sum[
		(*Putting the trace expression together with propagators of the relevant type*)
		totalXOrder= Sum[xOrders[[a, openDevs[[a]] +1]], {a, Length@ Xs}];

		(*Xt[fields, open derivatives, extraDevs from tilde expansion]*)
		pos= 0;
		expr= NonCommutativeMultiply@@ Table[pos++;
			\[CapitalDelta]t[x[[1, 1]], 0]** Xt[{x[[1, 1]], x[[1, 2]]}, openDevs[[pos]], 0]
			,{x, Xs}];

		(*Expanding the term up to the given number of derivatives*)
		expr= Sum[
			temp= expr;
			temp[[;;, -1]]+= devs;
			temp
		,{devs, IntegerSets[order -totalXOrder, Length@ expr]}];

		expr= TildeExpand@ expr;

		(*Performing the momentum integration and various simplifications*)
		expr= expr// PerformMomDerivatives// CollectGammaMatrices// ExtractMomenta// CollectPropagators// LorentzSimplify// CanonizeIndices;
		
		(*Put indices and type on Gs*)
		expr= MakeGLabels@ expr;

		(* Substitute the X[{_,_}] and canonize the indices *)
		expr= CanonizeIndices@ ContractDelta[
			CollectGammaMatrices@ RemoveHigherDimOps[Tr[expr/. XSubPattern@ subs]/. Tr@ x_-> x, cutOffDim]
			(*Tr[expr/. XSubPattern@ subs]/. Tr@ x_-> x*)
		];

		If[MatchQ[Xs[[1, 1, 1]], \[CapitalPsi]| \[Psi]],
			expr= expr// CloseFermionTrace;
		];

		expr= preFact expr// MultiScaleIntegrate// CollectGammaMatrices// RefineDiracProducts// EpsExpand//
			Expand// LorentzSimplify// SplitSymmetrizedDevs// CanonizeSpinorLines// RelabelDummyIndices// CanonizeIndices;

		CollectLTerms@expr/. {Log[a_] -Log[b_]-> Log[a/b], n_ Log[a_] +m_ Log[b_]/; m === -n-> n Log[a/b]}
	,{openDevs, devCases}]
]


(* ::Subsubsection:: *)
(*Auxiliary functions *)


MakeIndi@ n_Integer:= Symbol["$i"<> ToString@ n];


MakeGLabels@ expr_:= expr/. x_NonCommutativeMultiply:> MakeGLabels@ x;
MakeGLabels@ trTerm_NonCommutativeMultiply:= Module[
	{
		field = trTerm[[-1, 1, 2]], (*This picks the label of the last field in trTerm e.g. for trTerm=___**X[{a,b},___] it assumes the value b. *)
		ind = 1, (*start with index 1*)
		temp
	},

	temp= Table[
		Switch[op
		,_G, (*If the previous X was X[{a,b},___] then field is set to the value field=b*)
			op = op/. G[\[Mu]_li, \[Nu]_li]:> TakeSymDev[\[Nu], CovD[li[], G[{field@ MakeIndi@ ind++, field@ MakeIndi@ ind}], \[Mu]]]
		,_X,
			field= op[[1, 2]];
			op[[1]]= {op[[1, 1]]@ MakeIndi@ ind++, op[[1, 2]]@ MakeIndi@ ind};
			op
		,_,
			op
		]
	,{op, List@@trTerm}];
	temp[[-1, 1, 2, 1]]= MakeIndi@ 1; (*set the very last index to 1, sucht that we obatin a closed form*)
	NonCommutativeMultiply@@ temp
];


(* ::Text:: *)
(*For counting the dimension of operators*)


OpDim@ Transp@ x_:= OpDim[x];
OpDim@ Bar@ x_:= OpDim[x];
OpDim[G|_G]:= 2;
OpDim[Field[_,\[CapitalPhi],__]]:= 2;
OpDim[Field[_,\[Phi],__]]:= 1;
OpDim[Field[_,\[CapitalPsi],__]]:= 5/2;
OpDim[Field[_,\[Psi],__]]:= 3/2;
OpDim[Field[_,V,__]]:= 2;


OperatorDimension[term_]:=
	Plus@@ Cases[term, CovD[\[Mu]_li, op_, _]:> Length@\[Mu] +OpDim@op, Infinity]+
	Plus@@ Cases[term, TakeSymDev[\[Mu]_li, _]:>Length@\[Mu], Infinity];


RemoveHigherDimOps[expr_, dim_]:= Module[{temp= Expand@ expr},
	temp= If[Head@ temp === Plus,
		List@@ temp, 
		{temp}];
	Plus@@ DeleteCases[temp, _?(OperatorDimension@ # > dim &)]
];


(* ::Section::Closed:: *)
(*Dealing with fermions *)


(* ::Subsection::Closed:: *)
(*Utility functions*)


LSpinFieldQ@ CovD[_, Field[_, \[Psi]|\[CapitalPsi], ___], _]= True;
LSpinFieldQ@ CovD[_, Transp@ Bar@ Field[_, \[Psi]|\[CapitalPsi], ___], _]= True;
LSpinFieldQ@ _= False;


RSpinFieldQ@ CovD[_, Bar@ Field[_, \[Psi]|\[CapitalPsi], ___], _]= True;
RSpinFieldQ@ CovD[_, Transp@ Field[_, \[Psi]|\[CapitalPsi], ___], _]= True;
RSpinFieldQ@ _= False;


CummulativeSpin@ op_NonCommutativeMultiply:= Module[{lPos, rPos, temp},
	lPos= Position[op, _? LSpinFieldQ][[;;, 1]];
	rPos= Position[op, _? RSpinFieldQ][[;;, 1]];
	temp= ConstantArray[0, Length@ op];
	temp[[rPos]]= 1;
	temp[[lPos]]= -1;
	FoldList[Plus, temp]
]


(* ::Subsection::Closed:: *)
(*Closing Dirac trace in the supertrace *)


(*This will fail for e.g. TakeSymDev[\[Mu], \[Psi]** \[Psi]bar]*)


CloseFermionTrace@ expr_:= expr/. x_NonCommutativeMultiply:> CloseFermionTrace@ x;
CloseFermionTrace@ op_NonCommutativeMultiply:= Module[{cumSpinCount, diracProdPos},
	cumSpinCount= CummulativeSpin@ op;

	(*Test if fermion chain needs cyclic rearangement through the trace*)
	If[MemberQ[cumSpinCount, -1],
		(*Sign is from Grassmanian varaibles*)
		Return[ -RotateLeft[op, FirstPosition[cumSpinCount, -1]]];
	];

	(*With no spin chain needing closing, there is a Dirac trace over the main spin chain*)
	diracProdPos= Flatten@ Position[op, _DiracProduct, {1}];
	diracProdPos= DeleteCases[diracProdPos, _? (cumSpinCount[[#]] >0 &)];

	DiracTrace[DiracProduct@@ op[[diracProdPos]]/.
		x_DiracProduct:> Flatten@ x]* Delete[op, List/@ diracProdPos]
];


(* ::Subsection::Closed:: *)
(*Transposing closed fermion lines *)


CanonizeSpinorLines@ expr_:= expr/. x_NonCommutativeMultiply:> CanonizeSpinorLines@ x;
CanonizeSpinorLines@ op_NonCommutativeMultiply:= Module[{cumSpinCount, max, l, r, expr},
	(*Removes non-spinors from NCM*)
	If[\[Gamma]CommuteQ/@ Or@@ op,
		Return@ CanonizeSpinorLines[Replace[op, x_? \[Gamma]CommuteQ-> Commutative@ x, {1}]/. Commutative@ x_-> x];
	];

	cumSpinCount= CummulativeSpin@ op;
	max = Max@ cumSpinCount;

	(*If a closed spinor line is embedded in another they are split*)
	If[max > 1,
		{l, r}= First@ SequencePosition[cumSpinCount, {max..}, Overlaps-> False]+ {0, 1};
		Return@ CanonizeSpinorLines@ RefineDiracProducts@ CollectGammaMatrices[
			op[[;;l -1]]** op[[r+ 1;;]] op[[l;;r]]
		];
	];

	{l, r}= First@ SequencePosition[cumSpinCount, {max..}, Overlaps-> False]+ {0, 1};
	If[r < Length@ op,
		Return@ CollectGammaMatrices@ CanonizeSpinorLines[op[[;;r]] op[[r+1;;]] ];
	];

	If[MatchQ[op[[1]], CovD[_, Transp@ _Field, _]],
		Return[- CollectGammaMatrices@ Reverse[Transp/@ op]];
	];

	op
];

(* ::Package:: *)

Package["SuperTracer`"]


(* ::Title:: *)
(*SuperTracer`DiracAlgebra`*)


(* ::Subtitle:: *)
(*Implementation of the Dirac algebra used in SuperTracer.*)


(* ::Text:: *)
(*\[Gamma] matrices are all decomposed into the standard form (see e.g. https://arxiv.org/abs/physics/0703214, eq.  4.15) *)
(*DiracProduct[...,5,...] is interpreted as ...Subscript[\[Gamma], 5]... *)
(*DiracProduct[...,li@\[Alpha],...] is interpreted as ...Subscript[\[Gamma], \[Alpha]]...*)
(*DiracProduct[...,li[\[Mu],\[Nu]],...] is interpreted as ...Subscript[\[Sigma], \[Mu]\[Nu]]... in 4D*)
(*In NDR:  DiracProduct[..., li[Subscript[\[Mu], 1],...,Subscript[\[Mu], n]],...] is interpreted as \!\( *)
(*\*SubsuperscriptBox[\(\[CapitalGamma]\), \(n\), \(\( *)
(*\*SubscriptBox[\(\[Mu]\), \(1\)] ... \) *)
(*\*SubscriptBox[\(\[Mu]\), \(n\)]\)] = \(\( *)
(*\*SuperscriptBox[\(\[Gamma]\), \(\([\)*)
(*\*SubscriptBox[\(\[Mu]\), \(1\)]\)] ... \) *)
(*\*SuperscriptBox[\(\[Gamma]\), \( *)
(*\*SubscriptBox[\(\[Mu]\), \(n\)]\(]\)\)]\)\) (full antisymmetrization with normalization factor 1/n!)*)


(* ::Chapter:: *)
(*Public:*)


(* ::Section:: *)
(*Scoping:*)


(* ::Subsubsection:: *)
(*Exported*)


PackageExport["DiracProduct"]


PackageExport["\[Gamma]"]


PackageExport["Bar"]
PackageExport["Transp"]
PackageExport["CConj"]
PackageExport["CC"]


PackageExport["PL"]
PackageExport["PR"]
PackageExport["Proj"]


(* ::Subsubsection:: *)
(*Internal*)


PackageScope["CollectGammaMatrices"]


PackageScope["ASymGammaExpand"]


PackageScope["DiracTrace"]


PackageScope["RefineDiracProducts"]


PackageScope["\[Gamma]CommuteQ"]


(* ::Section:: *)
(*Usage definitions*)


DiracProduct::usage=
"DiracProduct[seq] represents the (non-commutative) product of Dirac matrices, charge conjugation matrices and chiral projectors in the sequence seq.";


\[Gamma]::usage=
"\[Gamma][\[Mu]] is a Dirac matrix with Lorentz index \[Mu].
\[Gamma][5] is the Dirac matrix \!\(\*SubscriptBox[\(\[Gamma]\), \(5\)]\).";


Bar::usage=
"Bar[field] returns the bar of a fermion field or the conjugate otherwise. Bar can also be applied to conjugate indices and charges.";

Transp::usage=
"Transp[arg] is used to denote the transposed of the object arg in the Dirac algebra.";

CConj::usage=
"CConj[field] is the charge conjugation operator and can be used to act only on fermion fields.";

CC::usage=
"CC is used as the matrix \!\(\*SuperscriptBox[\(\[ImaginaryI]\[Gamma]\), \(2\)]\)\!\(\*SuperscriptBox[\(\[Gamma]\), \(0\)]\) associated with charge conjugation and can only appearing inside a DiracProduct.";


Proj::usage=
"Proj[+/-] denotes the chiral right/left projectors \!\(\*SubscriptBox[\(P\), \(R/L\)]\) in a Dirac product.";

PL::usage=
"PL represents the chiral left projector \!\(\*SubscriptBox[\(P\), \(L\)]\) in a Dirac product.";

PR::usage=
"PR represents the chiral right projector \!\(\*SubscriptBox[\(P\), \(R\)]\) in a Dirac product.";


(* ::Text:: *)
(*--- Internal ---*)


CollectGammaMatrices::usage=
"CollectGammaMatrices[expr] collects all \[Gamma]-matrices in a DiracProduct and and moves them to the left inside of a NonCommutativeMultiply.";


DiracTrace::usage=
"DiracTrace[expr] gives the Diract trace of an expression.";

RefineDiracProducts::usage=
"RefineDiracProducts[expr] matches all Dirac product in an expression to the basis of completely anti-symmetrized products.";


(* ::Section:: *)
(*Formatting*)


Format[x_DiracProduct, StandardForm]:= Row[List@@ x /. {5-> Subscript[\[Gamma], 5], li@ \[Mu]_-> Subscript[\[Gamma], \[Mu]],
	li[\[Mu]_, \[Nu]__]-> Subscript["\[CapitalGamma]", Row@ {\[Mu], \[Nu]}]}];


Format[Bar@ x_Symbol, StandardForm]:= OverBar@ x;
Format[Bar@ Field[l_,r__], StandardForm]:= Field[OverBar@ l, r];


Format[Transp@ x_, StandardForm]:= DisplayForm@ SuperscriptBox[x, "T"];
Format[Transp@ x:DiracProduct[_, __], StandardForm]:= DisplayForm@ SuperscriptBox[RowBox@{"(", x, ")"}, "T"];


Format[Proj[1], StandardForm]:= Subscript["P","R"];
Format[Proj[-1], StandardForm]:= Subscript["P","L"];


(* ::Chapter:: *)
(*Private:*)


(* ::Section:: *)
(*SNDR scheme*)


(* ::Subsection::Closed:: *)
(*Convenient notation*)


(* ::Text:: *)
(*Define the "p-slash"*)


dot@OrderlessPatternSequence[p,\[Gamma]]:=\[Gamma]@p;


(*PL= 1/2 - DiracProduct[5] /2
PR= 1/2 + DiracProduct[5] /2 *)


PL= DiracProduct@ Proj@ -1;
PR= DiracProduct@ Proj@ 1;


\[Gamma]@ \[Mu]_li:= DiracProduct@ \[Mu];
\[Gamma]@ 5:= DiracProduct@ 5;
\[Gamma]@ \[Mu]:Except[p, _Symbol| _Symbol@_] := DiracProduct@ li@\[Mu];


(* ::Subsection::Closed:: *)
(*Properties of DiracProduct*)


(* ::Subsubsection:: *)
(*General properties*)


DiracProduct/:g@li@OrderlessPatternSequence[\[Mu]_,\[Nu]_] * DiracProduct[a___,li[\[Alpha]___,\[Mu]_,\[Beta]___],b___]:=DiracProduct[a,li[\[Alpha],\[Nu],\[Beta]],b]; (*Might be redundant*)
DiracProduct[a___,li[],b___]=DiracProduct[a,b];
DiracProduct[]=1;


(* ::Subsubsection:: *)
(*SNDR specific properties*)


(* ::Text:: *)
(*Subscript[\[Gamma], 5] : contract pairs and move singles to the right*)


DiracProduct[a___, 5, b___/; FreeQ[List@ b, 5| CC| Proj], 5, c___]:=
	(-1)^Length@ Flatten@ li@ b * DiracProduct[a, b, c];
DiracProduct[a___, 5, b___/; FreeQ[List@ b, 5| CC| Proj], Proj @s_, c___]:=
	s (-1)^Length@ Flatten@ li@ b * DiracProduct[a, b, Proj@ s, c];
DiracProduct[a___, Proj@ s_, b___/; FreeQ[List@ b, 5| CC| Proj], 5, c___]:=
	s (-1)^Length@ Flatten@ li@ b * DiracProduct[a, Proj@ s, b, c];
DiracProduct[a___, 5, b__/; FreeQ[List@ b, 5| CC| Proj]]:=
	(-1)^Length@ Flatten@ li@ b * DiracProduct[a, b, 5];


DiracProduct[a___, Proj@r_, b___/; FreeQ[List@ b, 5| CC| Proj], Proj@s_, c___]:=
	If[r s === (-1)^Length@ Flatten@ li@ b, DiracProduct[a, b, Proj@ s, c], 0];
DiracProduct[a___, Proj@ s_, b__/; FreeQ[List@ b, 5| CC| Proj]]:=
	DiracProduct[a, b, Proj[s (-1)^Length@ Flatten@ li@ b] ];


(* ::Text:: *)
(*Move contracted \[Gamma]-matrices together and eventually contract them.*)


DiracProduct[a___,li@\[Mu]_,li@\[Mu]_,b___]:= \[ScriptD] DiracProduct[a,b];
DiracProduct[a___,li@\[Mu]_,li@\[Nu]_,b___,li@\[Mu]_,c___]/; FreeQ[List[b, c], \[Nu]]:=
	2 g@ li[\[Mu], \[Nu]] DiracProduct[a, b, li@ \[Mu], c]- DiracProduct[a, li@ \[Nu], li@ \[Mu], b, li@ \[Mu], c];


DiracProduct[a___,p,p,b___]:=dot[p,p] * DiracProduct[a,b];
DiracProduct[a___,p,li@\[Nu]_,b___,p,c___]:=2p@li[\[Nu]] * DiracProduct[a,b,p,c] - DiracProduct[a,li@\[Nu],p,b,p,c];


(* ::Subsection::Closed:: *)
(*Dirac trace*)


(* ::Text:: *)
(*Anti-symmetric expansion of the \[Gamma]-matrices inside the DiracProduct*)


ASymGammaExpand@DiracProduct[a___,\[Mu]_li/;Length@\[Mu]>1,b___]:=Module[{permutations=Permutations@\[Mu]},
	1/Length@permutations * Signature@\[Mu] * Plus@@((Signature@# * ASymGammaExpand@DiracProduct[a,Sequence@@(li/@#),b]&)/@permutations)//Expand
];
ASymGammaExpand@x_=x;


(* ::Text:: *)
(*Anti-symmetrize all the DiracProducts inside DiracTraces*)


DiracTrace@DiracProduct[a___,li[\[Mu]_,\[Nu]__],b___]:=DiracTrace@ASymGammaExpand@DiracProduct[a,li[\[Mu],\[Nu]],b]//Expand;


DiracTrace@DiracProduct[a___, Proj@ s_]:= DiracProduct@ a/ 2+ s DiracProduct[a, 5]/2 //DiracTrace;


(* ::Text:: *)
(*Messages*)


DiracTrace::unkown="Dirac trace unknown: left unevaluated.";
DiracTrace::canteval="Dirac trace encountered term with multiple Dirac products. No tracing applied.";


(* ::Subsubsection:: *)
(*General properties*)


(* ::Text:: *)
(*Basic traces*)


DiracTrace@DiracProduct[li@\[Mu]_,li@\[Nu]_]=4g@li[\[Mu],\[Nu]];
DiracTrace@DiracProduct[li@\[Mu]_,li@\[Nu]_,li@\[Rho]_,li@\[Sigma]_]:=4g@li[\[Mu],\[Nu]]g@li[\[Rho],\[Sigma]] - 4g@li[\[Mu],\[Rho]]g@li[\[Nu],\[Sigma]] + 4g@li[\[Mu],\[Sigma]]g@li[\[Nu],\[Rho]];


(* ::Text:: *)
(*Move the DiracTrace from acting on any expression to act on the DiracProduct inside the expression*)


DiracTrace@expr_:=Module[
	{
		terms = Expand@expr,
		term
	},
	terms = If[Head@terms===Plus,
		List@@terms,
		{terms}
	];
	(*sum over all terms in the list terms*)

	Sum[
		Switch[Count[term,_DiracProduct,Infinity],
			0,4 term,
			1,term/.x_DiracProduct:>DiracTrace@x,
			_,Message[DiracTrace::canteval];term
		],
		{term,terms}
	]
];


(* ::Subsubsection:: *)
(*Semi-NDR specific properties*)


(* ::Text:: *)
(*Reduction of the DiracTraces*)


DiracTrace@x_DiracProduct:=Module[
	{
		indices,
		n,
		\[Alpha]
	},
	(*Reduce the traces depending on whether they include \[Gamma]^5 or not.*)
	Switch[MemberQ[List@@x,5],
		False,
			indices=List@@x[[;;,1]];
			If[OddQ@Length@x,Return@0;];
			Sum[
				(-1)^n * g[li@@indices[[{1,n}]]] * DiracTrace[x[[Complement[Range@Length@x,{1,n}]]] ],
				{n,2,Length@x}
			]//Expand,
		True,
			indices=li@@x[[;;-2,1]];
			If[EvenQ@Length@x||Length@x===3||Length@x===1,Return@0;];
			If[Length@x>=7,
				Message[DiracTrace::unkown];
				Return@x;
			]; (*Reduction has not yet been attempted. These will have evanescent contributions*)
			-4I * \[CurlyEpsilon]@indices
	]
];


(* ::Subsection:: *)
(*Expand to basis*)


(* ::Subsubsection:: *)
(*General Properties*)


RefineDiracProducts@ expr_:= expr/. x_DiracProduct:> RefineDiracProducts@ x// Expand;


(* ::Subsubsection:: *)
(*SNDR specific properties*)


(* ::Text:: *)
(*Anti-symmetrize all the DiracProducts inside DiracTraces. Performance can be improved with the techniques in [1905.00429], Sec 2.9.2.*)


RefineDiracProducts@ DiracProduct[a___, li[\[Mu]_, \[Nu]__], b___]:=
	RefineDiracProducts@ ASymGammaExpand@ DiracProduct[a, li[\[Mu], \[Nu]], b]// Expand;


(* ::Text:: *)
(*Expansion of Dirac product unto basis of anti-symmetric gamma matrices with the techniques of [1905.00429], Sec 2.9.2.*)


(*RefineDiracProducts::CC= "Diracproduct contains CC, please Check the X substitutions.";
RefineDiracProducts@ x:DiracProduct[CC,__]:= (Message[RefineDiracProducts::CC]; x);*)


RefineDiracProducts@ DiracProduct[CC]:= DiracProduct@ CC;
RefineDiracProducts@ DiracProduct[CC, x__]:=
	DiracProduct@ CC** RefineDiracProducts@ DiracProduct@x;


RefineDiracProducts@ DiracProduct@ 5:= DiracProduct@ 5;
RefineDiracProducts@ DiracProduct@ s_Proj:= DiracProduct@ s;
RefineDiracProducts@ x_DiracProduct:= Module[{gammas, prepend, k, set, pairs, aSym, pairCombs},
	If[MatchQ[Last@ x, 5| _Proj],
		gammas = li@@ x[[;;-2]]// Flatten;
		prepend= Last@ x;
	,
		gammas = li@@ x// Flatten;
		prepend= Sequence[];
	];
	Signature@ gammas Sum[
		aSym= Complement[gammas, set];
		pairCombs= DeleteDuplicatesBy[Partition[#, 2]&/@ Permutations@ set, (Sort[Sort/@ #] &)];
		Sum[
			Signature@ Flatten@ li[pairs, aSym]Times@@ g/@ pairs DiracProduct[aSym, prepend]
		,{pairs, pairCombs}]
	,{k, 0, Length@ gammas, 2}, {set, Subsets[gammas, {k}]}]

];


RefineDiracProducts::transp= "Some, but not all, matrices in `1` was transp. Could not match to basis";
RefineDiracProducts@ x:DiracProduct[_Transp, ___]:= Module[{},
	If[!MatchQ[x, DiracProduct[_Transp..]],
		Message[RefineDiracProducts::transp, x];
		Return@ x;
	];

	RefineDiracProducts@ Reverse[x][[;;, 1]]/. d_DiracProduct:> Transp@ d
];


(* ::Subsection:: *)
(*Collecting and manipulating the gamma matrices*)


(* ::Text:: *)
(*Check if something commutes with gamma matrices: *)


\[Gamma]CommuteQ[X|del|\[Gamma]|DiracProduct]=False;
\[Gamma]CommuteQ@ Field[_, (\[Psi]| \[CapitalPsi]), ___]= False;
\[Gamma]CommuteQ[f_?\[Gamma]CommuteQ[x__]]:=And@@\[Gamma]CommuteQ/@{x};
\[Gamma]CommuteQ[f_[x__]]:=False;
\[Gamma]CommuteQ@ Alternatives[Pattern, _Blank, _BlankSequence, _BlankNullSequence, _Except] = False;
\[Gamma]CommuteQ[_]=True; (*Dangerous?!*)


(* ::Text:: *)
(*Collect all gamma matrices in Dirac products on the left of the non-commutative multiply:*)


CollectGammaMatrices@expr_:=expr/.{
		\[Gamma]@x_   :> Hold@DiracProduct@x,
		x_DiracProduct :> Hold@ x
	}//.{
		NonCommutativeMultiply[a___,x_?\[Gamma]CommuteQ,Hold@g_DiracProduct,b___]:>NonCommutativeMultiply[a,Hold@g,x,b]
	}//.{
		NonCommutativeMultiply[a___, Hold@ DiracProduct@ \[Mu]__, Hold@ DiracProduct@ \[Nu]__, b___]:>
			NonCommutativeMultiply[a, Hold@DiracProduct[\[Mu], \[Nu]], b]
	}//ReleaseHold;


(* ::Subsection:: *)
(*Charge conjugation*)


(* ::Text:: *)
(*See e.g. [1412.3320] for the algebra with charge conjugation*)


(* ::Subsubsection:: *)
(*CC*)


DiracProduct[a___, CC, CC, b___]:= - DiracProduct[a, b];
DiracProduct[a___, 5, CC, b___]:= DiracProduct[a, CC, Transp@ 5, b];
DiracProduct[a___, s_Proj, CC, b___]:= DiracProduct[a, CC, Transp@ s, b];
DiracProduct[a___, \[Mu]_li, CC, b___]:= Power[-1, Ceiling[Length@ \[Mu]/2]] DiracProduct[a, CC, Transp@ \[Mu], b];


(* ::Subsubsection:: *)
(*Transp*)


Transp@ 0= 0; 


Transp@ CC= -CC;


Transp[x_?NumberQ y_]:= x Transp@ y;


(*Transp@ x_DiracProduct:= Reverse[Transp/@ x];*)
Transp@ CovD[\[Mu]_li, f_, \[Nu]_li]:= CovD[\[Mu], Transp@ f, \[Nu]];


Transp@ Transp@ x_:= x;


(* ::Subsubsection:: *)
(*Bar*)


Bar@ Bar@ x_:= x;


Bar@ CovD[\[Mu]_li, f_, \[Nu]_li]:= CovD[\[Mu], Bar@ f, \[Nu]];
Bar@ Transp@ f_:= Transp@ Bar@ f;
(*Special case of charge conjugation before Bar*)
Bar[DiracProduct@CC ** f:CovD[_, Transp@ Bar@ _Field, _] ]:= Bar@ f ** DiracProduct@ CC;


Bar@ l_List := Map[Bar,l]


(* ::Subsubsection:: *)
(*CConj*)


(* ::Text:: *)
(*Properties of charge conjugation working on fermion fields *)


CConj::notfermion= "CConj is only supposed to act on a fermion field."


CConj@ f: CovD[_li, Field[_, \[Psi]| \[CapitalPsi], __], _li]:= DiracProduct@ CC** Transp@ Bar@ f;
CConj@ f: CovD[_li, Bar@ Field[_, \[Psi]| \[CapitalPsi], __], _li]:= Transp@ Bar@ f ** DiracProduct@ CC;
CConj@ _:= (Message[CConj::notfermion]; Abort[]);

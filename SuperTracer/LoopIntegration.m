(* ::Package:: *)

Package["SuperTracer`"]


(* ::Title:: *)
(*SuperTracer`LoopIntegration`*)


(* ::Subtitle:: *)
(*Procedures for doing one-loop integrals with 0 external momenta  *)


(* ::Chapter:: *)
(*Public:*)


(* ::Section:: *)
(*Scoping:*)


(* ::Subsubsection:: *)
(*Exported*)


PackageExport["$DegenerateMasses"]


PackageExport["\[Mu]bar2"]


PackageExport["\[Epsilon]"] (*Do we need to export this?*)


PackageExport["EvaluateLoopFunctions"]
PackageExport["LF"]


(* ::Subsubsection:: *)
(*Internal*)


PackageScope["EpsExpand"]
PackageScope["MultiScaleIntegrate"]
PackageScope["LogTermIntegrate"]
PackageScope["SymTensor"]


(* ::Section:: *)
(*Usage definitions*)


$DegenerateMasses::usage= 
"$DegenerateMasses can be set to True/False (default False). If True, the loop integrals assume degenerate masses of the heavy states.";


\[Mu]bar2::usage=
"\[Mu]bar2 is the matching scale squared \!\(\*SuperscriptBox[OverscriptBox[\(\[Mu]\), \(_\)], \(2\)]\).";


\[Epsilon]::usage=
"\[Epsilon] is the parameter defined by expanding the space-time dimension \[ScriptD] around four dimensions, i.e. \[ScriptD]=4-2\[Epsilon].";


LF::usage=
"LF[{masses of propagators (m1,...)}, {propagator powers (n1,...)}] denotes the finite part of the loop integral with propagators \!\(\*FractionBox[\(1\), \(\(D \*SuperscriptBox[\((m1)\), \(n1\)] ... \) D \*SuperscriptBox[\((mk)\), \(nk\)] D \*SuperscriptBox[\((0)\), \(n \((k + 1)\)\)]\)]\), where the last propagator is always massless.";


EvaluateLoopFunctions::usage=
"EvaluateLoopFunctions[expr] evaluates all loop functions (LF) in an expression."


(* ::Section:: *)
(*Formatting *)


Format[\[Mu]bar2, StandardForm]:= OverBar["\[Mu]"]^2;


Format[LF[Masses_, powers_], StandardForm]:= Subscript[LF, Sequence@@ powers]@@ Masses;


(* ::Chapter:: *)
(*Private:*)


(* ::Section:: *)
(*Auxiliary functions*)


TermsToList@ expr_:= Module[{temp= Expand@ expr},
	If[Head@ temp === Plus, List@@ temp, List@ temp]
];


SymTensor[inds_li]:= If[EvenQ@ Length@ inds, 
	Plus@@ Times@@@ Map[g, DeleteDuplicatesBy[Partition[#, 2]&/@ Permutations@ inds, (Sort[Sort/@ #] &)], {2}]
	,0];


EpsExpand@ expr_:= Module[{sub},
	Normal@ Series[expr/. \[ScriptD]-> 4-2\[Epsilon](*/. {Power[\[Mu]bar2/ m_M^2, \[Epsilon]]-> (1 +\[Epsilon] Log[\[Mu]bar2 /m^2]),
		Power[\[Mu]bar2/ M^2, \[Epsilon]]-> (1+ \[Epsilon] Log[\[Mu]bar2/ M^2])}*), {\[Epsilon], 0, 0}] ];


(* ::Section:: *)
(*Multiscale integration*)


$DegenerateMasses= True; (*Flag for multi-/single-scale integration*)


integralType/: integralType[inds_, \[Alpha]_, li[\[Mu]___]] p@ li@ \[Nu]_:= integralType[inds, \[Alpha], li[\[Mu], \[Nu]]];
integralType/: integralType[inds_, \[Alpha]_, li[\[Mu]___]] \[CapitalDelta]@ 0:= integralType[inds, \[Alpha]-1, li[\[Mu]] ];
integralType/: integralType[inds_, \[Alpha]_, li[\[Mu]___]] Power[\[CapitalDelta]@ 0, n_]:= integralType[inds, \[Alpha]-n, li[\[Mu]]];
integralType/: integralType[inds_, \[Alpha]_, li[\[Mu]___]] Power[\[CapitalDelta]@ x_, n_]:= integralType[Merge[{inds, <|x-> -n|>}, Total], \[Alpha], li[\[Mu]]];


SingleScaleIntegral[{mass_, \[Beta]_Integer}, \[Alpha]_Integer, li[]]:= Module[{preFact},
	If[\[Beta] <= 0, Return@ 0;];
	preFact= (-1)^(\[Alpha]+\[Beta]) I mass^(2(2- \[Alpha]- \[Beta]))/ Gamma@ \[Beta];

	If[\[Alpha] >= 2,
		Return[-preFact Gamma[\[Alpha]+ \[Beta]- 2] (-1)^\[Alpha]/ (\[Alpha]-2)!
		(1/\[Epsilon] -HarmonicNumber[\[Alpha]-2]+ Log[\[Mu]bar2/ mass^2]+ 1+ PolyGamma[\[Alpha]+\[Beta]-2]+ EulerGamma) ];
	];
	If[\[Alpha]+ \[Beta] <= 2,
		Return[preFact Gamma[2- \[Alpha]] (-1)^(\[Alpha]+\[Beta])/(2-\[Alpha]-\[Beta])! 
		(1/\[Epsilon] +HarmonicNumber[2-\[Alpha]-\[Beta]]+ Log[\[Mu]bar2/ mass^2]+ 1- PolyGamma[2- \[Alpha]]- EulerGamma)];
	];
	preFact Gamma[2- \[Alpha]] Gamma[\[Alpha]+ \[Beta]- 2] 
];
SingleScalePole[{mass_, \[Beta]_Integer}, \[Alpha]_Integer, li[]]:= Module[{preFact},
	If[\[Beta] <= 0, Return@ 0;];
	preFact= (-1)^(\[Alpha]+\[Beta]) I mass^(2(2- \[Alpha]- \[Beta]))/ Gamma@ \[Beta];

	If[\[Alpha] >= 2,
		Return[-preFact Gamma[\[Alpha]+ \[Beta]- 2] (-1)^\[Alpha]/ (\[Alpha]-2)! 1/\[Epsilon]];
	];
	If[\[Alpha]+ \[Beta] <= 2,
		Return[preFact Gamma[2- \[Alpha]] (-1)^(\[Alpha]+\[Beta])/(2-\[Alpha]-\[Beta])! 1/\[Epsilon]];
	];
	0
];


(*SingleScaleIntegral[{mass_, \[Beta]_}, \[Alpha]_, inds_li]:= Module[{k= Length@ inds/2}, 
	((-1)^(\[Alpha]+\[Beta]+k) I) SymTensor@ inds mass^(2 (2+k-\[Alpha]-\[Beta])) 
		((\[Mu]bar2 Exp@ EulerGamma)/ (mass^2))^\[Epsilon] (Gamma[2+k-\[Alpha]-\[Epsilon]] Gamma[\[Alpha]+\[Beta]-2-k+\[Epsilon]])/(2^k Gamma[\[Beta]] Gamma[2+k-\[Epsilon]])
		];*)
MultiScaleIntegral[propPowers_Association, \[Beta]_, inds_li]:= Module[{i, p, j, temp},
	Sum[temp= KeyDrop[propPowers, i];
		SingleScaleIntegral[{i, propPowers@ i -p}, \[Beta], inds] /p! D[Product[Power[M2@ i -M2@ j, -propPowers@ j],
			{j, Keys@ temp}], {M2@ i, p}]
	,{i, Keys@ propPowers}, {p, 0, propPowers@ i -1}]/. M2@ mass_:> mass^2
];
MultiScalePole[propPowers_Association, \[Beta]_, inds_li]:= Module[{i, p, j, temp},
	If[Plus@@ propPowers + \[Beta] > 2 && \[Beta] > 2, Return@ 0; ];
	Sum[temp= KeyDrop[propPowers, i];
		SingleScalePole[{i, propPowers@ i -p}, \[Beta], inds] /p! D[Product[Power[M2@ i -M2@ j, -propPowers@ j],
			{j, Keys@ temp}], {M2@ i, p}]
	,{i, Keys@ propPowers}, {p, 0, propPowers@ i -1}]/. M2@ mass_:> mass^2
];


MultiScaleIntegrate[expr_]:= Module[{temp, term, masses},
	temp= expr/. {Power[Pattern[x, Blank@\[CapitalDelta]], n_]:> Commutative@ Power[x, n], 
			x_M-> Commutative@ x}/. Commutative@ x_-> x;
	If[$DegenerateMasses,
		temp= temp/. x_M-> M@ "H";
	];
	
	temp= temp integralType[<||>, 0, li[]]// TermsToList; 
	
	Sum[term/. x_integralType:> ToLoopFunctions@@ x, {term, temp}]
];


(* ::Subsubsection:: *)
(*Loop functions*)


ToLoopFunctions[propPowers_Association, \[Beta]_, li[\[Mu]__]]:= MultiScaleIntegral[propPowers, \[Beta], li@ \[Mu]];
ToLoopFunctions[propPowers_Association, \[Beta]_, li[]]:= Module[{props},
	props= SortBy[KeyValueMap[{#1, #2}&, propPowers], (-#[[2]]&)];
	I LF[props[[;;, 1]], props[[;;, 2]] ~ Join ~ {\[Beta]}] + Simplify@ MultiScalePole[propPowers, \[Beta], li[]] 
]


TestLFArgs[denoms_, powers_]:= 
	If[!MatchQ[denoms, _List] || !MatchQ[powers, {_Integer..}] || (Length@ denoms+ 1 =!= Length@ powers), 
		Message[LF::args]; Abort[]; 
	];


EvaluateLoopFunctions@ LF[denoms_, powers_]:= Module[{association},
	TestLFArgs[denoms, powers];
	association= Association@@ (#[[1]]->#[[2]]&)/@ Transpose@ {denoms, powers[[;;-2]]};
	-I* (MultiScaleIntegral[association, Last@ powers, li[]]- 
		MultiScalePole[association, Last@ powers, li[]])// Simplify
];
EvaluateLoopFunctions@ expr_:= expr/. lf_LF:> EvaluateLoopFunctions@ lf// Simplify;


LF::args= "Arguments of LF are invalid."


LF[props_List, powers:{_Integer..}]/; (Length@ props+ 1 =!= Length@ powers):=
	(Message[LF::args]; Abort[];); 


(* ::Section:: *)
(*Log term integration*)


LogTermIntegrate[expr_]:= Module[{temp, term, masses},
	temp= expr/. {Power[Pattern[x, Blank@\[CapitalDelta]], n_]:> Commutative@ Power[x, n], 
			x_M-> Commutative@ x}/. Commutative@ x_-> x;
	If[$DegenerateMasses,
		temp= temp/. x_M-> M@ "H";
	];
	
	temp= temp integralType[<||>, 0, li[]]// TermsToList; 
	
	Sum[term/.<|m_->\[Beta]_|>:>{m,\[Beta]} /. x_integralType:> LogTermIntegral@@ x, {term, temp}]
];


LogTermIntegral[{mass_, \[Beta]_Integer}, \[Alpha]_Integer, li[]]:= Module[{},
	If[\[Beta] <= 0 || \[Alpha] >= 2 || \[Alpha]+ \[Beta] <= 2,
		Abort[];
	];
	(-1)^(\[Alpha]+\[Beta]) I mass^(2(2- \[Alpha]- \[Beta]))/ Gamma@ \[Beta] Gamma[2- \[Alpha]] Gamma[\[Alpha]+ \[Beta]- 2] (1+ (Log[\[Mu]bar2/ mass^2]+ 1+ PolyGamma[\[Alpha]+\[Beta]-2]- PolyGamma[2- \[Alpha]])\[Epsilon] )
];

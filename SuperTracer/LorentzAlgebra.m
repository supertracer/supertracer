(* ::Package:: *)

Package["SuperTracer`"]


(* ::Title:: *)
(*SuperTracer`LorentzAlegra`*)


(* ::Subtitle:: *)
(*Implementation of the Lorentz algebra.*)


(* ::Chapter:: *)
(*Public:*)


(* ::Section:: *)
(*Scoping:*)


(* ::Subsubsection:: *)
(*Exported*)


PackageExport["\[CurlyEpsilon]"]


PackageExport["li"]


PackageExport["CanonizeIndices"]


PackageExport["g"]


(* ::Subsubsection:: *)
(*Internal*)


PackageScope["\[ScriptD]"]


PackageScope["UniqueLorentz"]


PackageScope["LorentzSimplify"]


(* ::Section:: *)
(*Usage definitions*)


\[CurlyEpsilon]::usage=
"\[CurlyEpsilon][li[\[Mu],\[Nu],\[Rho],\[Sigma]]] is the Levi-Civita tensor with Lorentz indices \[Mu], \[Nu], \[Rho], \[Sigma].";


li::usage=
"li[\[Mu], \[Nu], ...] denotes a sequence of Lorentz indices \[Mu], \[Nu], ... .";


CanonizeIndices::usage=
"CanonizeIndices[expression] brings all the Lorentz indices in the expression into canonical order.";


(* ::Text:: *)
(*--- Internal ---*)


g::usage=
"g[li[\[Mu],\[Nu]]] is the Minkowski metric with Lorentz indices \[Mu] and \[Nu].";


\[ScriptD]::usage= 
"The \[ScriptD]=4-2\[Epsilon] is the dimension of the continued spacetime.";


LorentzSimplify::usage=
"LorentzSimplify[expression] simplifies the strucucture of all Lorentz indices in the expression.";


(* ::Section:: *)
(*Formatting*)


Format[g@ li[\[Mu]_, \[Nu]_], StandardForm]:= Subscript[g, Row@ {\[Mu],\[Nu]}];
Format[\[CurlyEpsilon]@ li[\[Mu]_, \[Nu]_, \[Rho]_, \[Sigma]_], StandardForm]:= Subscript[\[CurlyEpsilon], Row@ {\[Mu], \[Nu], \[Rho], \[Sigma]}];


(* ::Chapter:: *)
(*Private:*)


(* ::Section:: *)
(*Metric*)


(* ::Subsection:: *)
(*Set attribute orderless*)


(* Do we actually need this, since we will always have g@li[___] ??? I guess setting li to orderless will break stuff? *)
SetAttributes[g,Orderless]


(* ::Subsection:: *)
(*Self contractions*)


g@li[\[Mu]_,\[Mu]_]=\[ScriptD];
g/:Power[_g,2]=\[ScriptD];


(* ::Subsection:: *)
(*Other contractions*)


(* 
	The first line seems redundant, i.e. included in line 3. 
	We do not yet cover the case when li[___] is not at level 1. Can that happen? 
*)
g/:g@li@OrderlessPatternSequence[\[Mu]_,\[Nu]_]*g@li@OrderlessPatternSequence[\[Nu]_,\[Rho]_]:=g@li[\[Mu],\[Rho]];
g/:g@li@OrderlessPatternSequence[\[Mu]_,\[Nu]_]*NonCommutativeMultiply[a___,h_[i___,li[\[Mu]1___,\[Nu]_,\[Mu]2___],j___],b___]:=NonCommutativeMultiply[a,h[i,li[\[Mu]1,\[Mu],\[Mu]2],j],b];
g/:g@li@OrderlessPatternSequence[\[Mu]_,\[Nu]_]*h_[i___,li[\[Mu]1___,\[Nu]_,\[Mu]2___],j___]:=h[i,li[\[Mu]1,\[Mu],\[Mu]2],j];
g/:g@li@OrderlessPatternSequence[\[Mu]_,\[Nu]_]*NonCommutativeMultiply[a___,Transp@ h_[i___,li[\[Mu]1___,\[Nu]_,\[Mu]2___],j___],b___]:=NonCommutativeMultiply[a,Transp@ h[i,li[\[Mu]1,\[Mu],\[Mu]2],j],b];


(* ::Subsection:: *)
(*Further functionality that might be useful*)


g[\[Mu]_,\[Nu]_]:=g@li[\[Mu],\[Nu]]


(* ::Section:: *)
(*The Levi-Civita*)


\[CurlyEpsilon]@li@OrderlessPatternSequence[\[Mu]_,\[Mu]_,__]=0;


\[CurlyEpsilon][a_, b_, c_, d_]:= \[CurlyEpsilon]@li[a, b, c, d];


(* ::Section:: *)
(*Bring Lorentz indices to canonical order*)


(* ::Subsection:: *)
(*Canonize the Lorentz indices*)


UniqueLorentz::usage="UniqueLorentz[expr]: Replaces all repeated Lorentz indices in expr with unique labels.";


(*make UniqueLorentz apply separately to every entry of a list*)
UniqueLorentz[l_List]:=Map[UniqueLorentz, l]


(*This function finds all repeated Lorentz indices and gives them unique labels*)
UniqueLorentz[arg_]:=Module[
	{
		expr = Expand@arg,
		terms,
		indices,
		replacements
	},
	
	(* Check whether expr is a sum *)
	If[Head@expr===Plus,
		terms = List@@expr,
		terms = List@expr
	];
	
	(*sum over all terms*)
	Sum[
		(*find indices*)
		indices = Cases[RemovePower@term, li[seq__]:>seq, All];
		
		(*check for dummies*)
		indices = DeleteCases[Tally@indices,_?(Last@#=!=2&)][[;;,1]];
		
		(*create replacement rules to unique symbols*)
		replacements = Table[ind->Unique[\[Mu]aux], {ind,indices}];
		
		(*apply rules*)
		term/.replacements
		,
		{term,terms}
	]
]


(*make CanonizeIndices apply separately to every entry of a list*)
CanonizeIndices[l_List]:=Map[CanonizeIndices, l]


CanonizeIndices[expr_]:=Module[
	{
		indices,
		(*First make all Lorentz dummy indices unique to avoid double usage of symbols*)
		temp=Expand@UniqueLorentz@expr,
		terms,
		replacement,
		replacements,
		p,
		trails
	},
	
	(* Check whether temp is a sum *)
	If[Head@temp===Plus,
		terms = List@@temp,
		terms = List@temp;
	];
	
	(*sum over all terms*)
	Sum[
		(*get a list of all Lorentz indices in a single term*)
		indices = Cases[RemovePower@term, li[seq__]:>seq, All];
		
		(* 
		(* old version *)
		indices=List@@Join@@Table[
			term[[Sequence@@p]],
			{p,Position[term,li@__]}
		];
		*)
		
		(*Extract the dummy indices*)
		indices=DeleteCases[Tally@indices,_?(Last@#=!=2&)][[;;,1]];
		 
		(*create the replacement rules for any permutation of index labels*)
		replacements=MapIndexed[(#1->Symbol["$\[Mu]"<>ToString@First@#2]&),#]&/@Permutations@indices;
		
		(*Apply the replacement rules for every permutation separately, and sort the indices.*)
		trails=Table[
			term/.replacement/.{
				G[\[Mu]_li,\[Rho]_li] :> Signature@\[Mu] * G[Sort@\[Mu],Sort@\[Rho]],
				X[op_,\[Mu]_li,\[Nu]_] :> X[op,Sort@\[Mu],\[Nu]],
				g@\[Mu]_li :> g@Sort@\[Mu],
				DiracProduct[\[Mu]_li]:> Signature@ \[Mu] * DiracProduct@ Sort@\[Mu],
				DiracProduct[\[Mu]_li, ch:(5|_Proj)] :> Signature@ \[Mu] * DiracProduct[Sort@\[Mu], ch],
				\[CurlyEpsilon]@\[Mu]_li :> Signature@\[Mu] * \[CurlyEpsilon]@Sort@\[Mu],
				CovD[\[Mu]_li, g:G|_G, \[Nu]_li] :> Signature@\[Nu] * CovD[\[Mu],g,Sort@\[Nu]]
			},
			{replacement,replacements}
		];
		(*The index ordering can flip signs and thus trip up the ordering function, whence Abs is applied*)
		(*Find the permutation which leads to the lowest value for Sort.*)
		trails[[ First@Ordering[Abs@trails,1] ]]
		,
		{term,terms}
	]
];


(* ::Subsection:: *)
(*Formatting of the internal Lorentz dummy indices*)


$LorentzAlphabet = {"\[Mu]", "\[Nu]", "\[Rho]", "\[Sigma]", "\[Omega]", "\[Alpha]", "\[Beta]"};


Do[
	Format[Symbol["Global`$\[Mu]"<>ToString@n], StandardForm] = Module[
		{aux, letter, counter},
		aux = QuotientRemainder[n-1, Length@$LorentzAlphabet];
		letter = $LorentzAlphabet[[ aux[[2]] + 1 ]];
		counter = aux[[1]];
	
		If[counter==0,
			letter,
			Subscript[letter,counter]
		]
	]
	,
	{n, 50}
]


(* ::Section:: *)
(*Lorentz Index Simplification*)


LorentzSimplify@expr_:=Module[
	{
		out
	},
	out=expr//.{
		Times[p@li@\[Mu]1_,p@li@\[Mu]2_] * HoldPattern@NonCommutativeMultiply[___,G[li[\[Mu]1_,\[Mu]2_],_],___]->0,
		HoldPattern@NonCommutativeMultiply@OrderlessPatternSequence[
			G[li@OrderlessPatternSequence[\[Mu]1_,\[Mu]2_],___],
			X[_,li@OrderlessPatternSequence[\[Mu]1_,\[Mu]2_,___],_li],
			___
		]->0,
		(*contracting the \[CurlyEpsilon] with symmetric index structures:*)
		\[CurlyEpsilon]@li@OrderlessPatternSequence[\[Mu]_,\[Nu]_,__] p@li@\[Mu]_ p@li@\[Nu]_->0,
		\[CurlyEpsilon]@li@OrderlessPatternSequence[\[Mu]_,\[Nu]_,__]HoldPattern@NonCommutativeMultiply[___,X[_,li@OrderlessPatternSequence[\[Mu]1_,\[Mu]2_,___],_li],___]->0,
		\[CurlyEpsilon]@li@OrderlessPatternSequence[\[Mu]_,\[Nu]_,__]HoldPattern@NonCommutativeMultiply[___,G[_li,li@OrderlessPatternSequence[\[Mu]1_,\[Mu]2_,___]],___]->0,
		Times[p@li@\[Mu]1_,p@li@\[Mu]2_] * HoldPattern@NonCommutativeMultiply[___,DiracProduct@\[Sigma]@li@OrderlessPatternSequence[\[Mu]1_,\[Mu]2_],___]->0
	}
];
